import java.io.*;
import java.nio.file.Path;
import static java.lang.System.*;

import org.junit.jupiter.api.*;
import static org.junit.jupiter.api.Assertions.*;

import cool.micro.clif.SourceReader;
import static cool.micro.clif.Utils.*;
import static cool.micro.clif.MyExceptions.*;
import static cool.micro.clif.BLIFWriter.*;


class MainTest {
    private InputStream openResource(String res) {
        ClassLoader classloader = Thread.currentThread().getContextClassLoader();
        InputStream inputStream = classloader.getResourceAsStream(res);
        assert inputStream != null;
        return inputStream;
    }

    private void printContent(InputStream res) {
        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(res));
            while (reader.ready()) {
                String line = reader.readLine();
                err.println(line);
            }
            err.println();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void run(String file) {
        err.println();
        err.println("___[" + file + "]_________");
        //printContent(file);
        err.flush();

        var read = new SourceReader();
        read.readFile(Path.of("res", file).toString());
        var merged = BLIFStringWriter.writeAllMerged(read.getMain());

        out.println("-----output-----");
        out.print(merged.getBuffer().toString());
        out.println("------end-------");
        out.flush();
    }
    private void runfail(String file) {
        runfail(file, RuntimeException.class);
    }
    private void runfail(String file, Class<? extends Throwable> exc) {
        assertThrows(exc, () -> {
            try {
                run(file);
            } catch (Exception e) {
                var em = e.getMessage();
                err.printf("---raised--- %s%s\n", clsName(e),
                    em == null || em.equals("") ? "" : (": " + em)
                );
                throw e;
            }
        });
    }
    
    @Test void bad_syntax1() { runfail("bad/syntax1.clif", ParsingError.class); }
    @Test void bad_recur1() { runfail("bad/include-recur/a.clif", RecursiveInclusionEx.class); }
    @Test void bad_recur2() { runfail("bad/modrecur1.clif", RecursiveInclusionEx.class); }
    @Test void bad_recur3() { runfail("bad/modrecur2.clif", RecursiveInclusionEx.class); }
    
    @Test void good_basemodel() { run("good/include-base/a.clif"); }
    @Test void good_subckt1() { run("good/subckt1.clif"); }
    @Test void good_connect1() { run("good/connect1.clif"); }
    @Test void good_difftypes1() { run("good/difftypes1.clif"); }
}
