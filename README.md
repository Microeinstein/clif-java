# CLIF

Cool Logic Interchange Format

## Introduction

This is my university thesis project — everything is work in progress. It consists of a new language and a source-to-source transpiler targeting BLIF (2005 version), mainly to allow better writing of exam projects in SIS.

### Examples

* multiple kinds of comments

    ```php
    # single
    // single
    /* multi
    */
    ```
    
* reassignment, direct copy

    ```java
    model mod(int2 a, int2 b) -> int2 ret {
        ret = a;
        ret = b;
    }
    ```
    
    ```coffeescript
    .model   mod
    .inputs  a1 a2 \
             b1 b2
    .outputs ret1 ret2
    
    # ret = b
    .names b1 ret1
    1 1
    .names b2 ret2
    1 1
    .end
    ```

* subcircuit, temporary and direct outputs, alias, empty bodies

  ```java
  model mod(int2 a, int2 b) -> int2 ret {
      s1 = new sub1(a);
      s2 = new sub2(s1.out, b);
      my = s2.out;
      ret = my;
  }
  
  model sub1(int2 a)         -> int2 out;
  model sub2(int2 a, int2 b) -> int2 out;
  ```

  ```coffeescript
  .model   mod
  .inputs  a1 a2 \
           b1 b2
  .outputs ret1 ret2
  
  .subckt sub1 \
      a1=a1 a2=a2 \
      out1=1.out1 out2=1.out2  # temp name here
  
  .subckt sub2 \
      a1=1.out1 a2=1.out2 \
      b1=b1 b2=b2 \
      out1=ret1 out2=ret2  # direct output here
  .end
  
  
  # placeholder for "sub1" model
  
  
  # placeholder for "sub2" model
  ```

## Planned features

* **Toolkit**
  * [x] source generation
  * [x] cross-platform
  * [ ] native
* **Language**
  * [x] multiple kinds of comments
  * [x] `include other.clif;`
  * [x] dynamic binding
  * [x] alias
  * [x] empty body
  * [x] subcircuit
    * [x] optimizations
  * [x] builtin types: `[bool] int_`
  * [x] constant
  * [ ] expressions
    * [ ] slice
    * [ ] concat
    * [ ] logic
    * [ ] math
  * [ ] latch
    * [ ] reading
    * [ ] writing
* **Language extras**
  * [ ] controller graph
  * [ ] `if`-mux conversion
  * [ ] bash list expand: `a{b,c}`
  * [ ] bash glob expand: `a*`
  * [ ] custom types: `struct union`


## Prerequisites

* Java 17
* ANTLR4 (manual invocation of automatic generation for parser+visitor)
* Gradle

## Repository structure

| **Directory** | **Description**                                        |
| ------------- | ------------------------------------------------------ |
| `src/`        | project sources                                        |
|   `lang/`       |  ANTLR4 parser + lexer grammars                         |
|   `gen/`        |  ANTLR4 generated parser + visitor                      |
|   `project/`    |  main project                                           |
|   `test/`       |  test sources                                           |
| `res/`        | test CLIF sources (_not embedded as actual resources_) |
|   `bad/`        |  that must fail                                         |
|   `good/`       |  that must compile                                      |
|   `elab2018/`   |  personal 2018 SIS project rewritten in _desired_ CLIF  |

