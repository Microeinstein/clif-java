lexer grammar CLIFLexer;

// keywords
// PASS : 'pass';
INCLUDE  : 'include' WS  -> pushMode(file_path);
STRUCT   : 'struct';
UNION    : 'union';
// OPERATOR : 'operator';
MODEL    : 'model';
KISS     : 'kiss';
STATIC   : 'static';
NEW      : 'new';
IF       : 'if';
ELSE     : 'else';
CASE     : 'case';
BREAK    : 'break';
ALL      : 'all';
ANY      : 'any';
BOOL     : 'true' | 'false';

// symbols
AT     : '@';
LPAR   : '(';
RPAR   : ')';
LBRAK  : '[';
RBRAK  : ']';
LBRACE : '{';
RBRACE : '}';
ARROW  : '->';
COLON  : ':';
COMMA  : ',';
SEMIC  : ';';
CONCAT : '..';
DOT    : '.';
SET    : '=';
QUEST  : '?';  // mux

// binary
NOT    : '!';
AND    : '&';
OR     : '|';
XOR    : '^';
LSHIFT : '<<';
RSHIFT : '>>';

// comparison
GREAT  : '>';
LESS   : '<';
EQU    : '==';  // xnor
NEQ    : '!=';  // xor
GEQ    : '>=';
LEQ    : '<=';

// math
PLUS   : '+';
MINUS  : '-';
AST    : '*';  // glob
DIV    : '/';
MOD    : '%';

// literals
NAME  : [A-Za-z_][0-9A-Za-z_]*;
HEXAD : '-'? '0' [xX] [0-9A-Fa-f_]+;
OCTAL : '-'? '0' [oO] [0-7]+;
BINAR : '-'? '0' [bB] [01_]+;
DECIM : '-'? [0-9]+;
RAW   : '"' ('\\' . | ~[\\"])* '"';


// control
SKIP_: (NL | WS | LINE_JOIN | LINE_COM | BLOCK_COM) -> skip;
LINE_JOIN : '\\' NL;
LINE_COM  : ('#' | '//') ~[\n\r]* NL?;
BLOCK_COM : '/*' .*? '*/';
NL: '\r\n' | '\r' | '\n';

// partial
fragment WS: [ \t\f]+ ;


mode file_path;
FP_SKIP_ : (LINE_JOIN | LINE_COM | BLOCK_COM) -> skip;
FP_SEMIC : SEMIC+ -> popMode;
FILEPATH : ~[\n\r]* ~[;\n\r];
