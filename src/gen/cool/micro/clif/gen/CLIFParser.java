// Generated from java-escape by ANTLR 4.11.1
package cool.micro.clif.gen;
import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.misc.*;
import org.antlr.v4.runtime.tree.*;
import java.util.List;
import java.util.Iterator;
import java.util.ArrayList;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast", "CheckReturnValue"})
public class CLIFParser extends Parser {
	static { RuntimeMetaData.checkVersion("4.11.1", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		INCLUDE=1, STRUCT=2, UNION=3, MODEL=4, KISS=5, STATIC=6, NEW=7, IF=8, 
		ELSE=9, CASE=10, BREAK=11, ALL=12, ANY=13, BOOL=14, AT=15, LPAR=16, RPAR=17, 
		LBRAK=18, RBRAK=19, LBRACE=20, RBRACE=21, ARROW=22, COLON=23, COMMA=24, 
		SEMIC=25, CONCAT=26, DOT=27, SET=28, QUEST=29, NOT=30, AND=31, OR=32, 
		XOR=33, LSHIFT=34, RSHIFT=35, GREAT=36, LESS=37, EQU=38, NEQ=39, GEQ=40, 
		LEQ=41, PLUS=42, MINUS=43, AST=44, DIV=45, MOD=46, NAME=47, HEXAD=48, 
		OCTAL=49, BINAR=50, DECIM=51, RAW=52, SKIP_=53, LINE_JOIN=54, LINE_COM=55, 
		BLOCK_COM=56, NL=57, FP_SKIP_=58, FP_SEMIC=59, FILEPATH=60;
	public static final int
		RULE_source = 0, RULE_directive = 1, RULE_inclusion = 2, RULE_num = 3, 
		RULE_varlist = 4, RULE_var = 5, RULE_ref = 6, RULE_member = 7, RULE_slice = 8, 
		RULE_declare = 9, RULE_struct = 10, RULE_union = 11, RULE_model = 12, 
		RULE_model_body = 13, RULE_model_stmt = 14, RULE_model_if = 15, RULE_args = 16, 
		RULE_arglist = 17, RULE_argmap = 18, RULE_argset = 19, RULE_exp = 20, 
		RULE_kiss = 21, RULE_kiss_body = 22, RULE_kiss_stmt = 23;
	private static String[] makeRuleNames() {
		return new String[] {
			"source", "directive", "inclusion", "num", "varlist", "var", "ref", "member", 
			"slice", "declare", "struct", "union", "model", "model_body", "model_stmt", 
			"model_if", "args", "arglist", "argmap", "argset", "exp", "kiss", "kiss_body", 
			"kiss_stmt"
		};
	}
	public static final String[] ruleNames = makeRuleNames();

	private static String[] makeLiteralNames() {
		return new String[] {
			null, null, "'struct'", "'union'", "'model'", "'kiss'", "'static'", "'new'", 
			"'if'", "'else'", "'case'", "'break'", "'all'", "'any'", null, "'@'", 
			"'('", "')'", "'['", "']'", "'{'", "'}'", "'->'", "':'", "','", "';'", 
			"'..'", "'.'", "'='", "'?'", "'!'", "'&'", "'|'", "'^'", "'<<'", "'>>'", 
			"'>'", "'<'", "'=='", "'!='", "'>='", "'<='", "'+'", "'-'", "'*'", "'/'", 
			"'%'"
		};
	}
	private static final String[] _LITERAL_NAMES = makeLiteralNames();
	private static String[] makeSymbolicNames() {
		return new String[] {
			null, "INCLUDE", "STRUCT", "UNION", "MODEL", "KISS", "STATIC", "NEW", 
			"IF", "ELSE", "CASE", "BREAK", "ALL", "ANY", "BOOL", "AT", "LPAR", "RPAR", 
			"LBRAK", "RBRAK", "LBRACE", "RBRACE", "ARROW", "COLON", "COMMA", "SEMIC", 
			"CONCAT", "DOT", "SET", "QUEST", "NOT", "AND", "OR", "XOR", "LSHIFT", 
			"RSHIFT", "GREAT", "LESS", "EQU", "NEQ", "GEQ", "LEQ", "PLUS", "MINUS", 
			"AST", "DIV", "MOD", "NAME", "HEXAD", "OCTAL", "BINAR", "DECIM", "RAW", 
			"SKIP_", "LINE_JOIN", "LINE_COM", "BLOCK_COM", "NL", "FP_SKIP_", "FP_SEMIC", 
			"FILEPATH"
		};
	}
	private static final String[] _SYMBOLIC_NAMES = makeSymbolicNames();
	public static final Vocabulary VOCABULARY = new VocabularyImpl(_LITERAL_NAMES, _SYMBOLIC_NAMES);

	/**
	 * @deprecated Use {@link #VOCABULARY} instead.
	 */
	@Deprecated
	public static final String[] tokenNames;
	static {
		tokenNames = new String[_SYMBOLIC_NAMES.length];
		for (int i = 0; i < tokenNames.length; i++) {
			tokenNames[i] = VOCABULARY.getLiteralName(i);
			if (tokenNames[i] == null) {
				tokenNames[i] = VOCABULARY.getSymbolicName(i);
			}

			if (tokenNames[i] == null) {
				tokenNames[i] = "<INVALID>";
			}
		}
	}

	@Override
	@Deprecated
	public String[] getTokenNames() {
		return tokenNames;
	}

	@Override

	public Vocabulary getVocabulary() {
		return VOCABULARY;
	}

	@Override
	public String getGrammarFileName() { return "java-escape"; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public ATN getATN() { return _ATN; }

	public CLIFParser(TokenStream input) {
		super(input);
		_interp = new ParserATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}

	@SuppressWarnings("CheckReturnValue")
	public static class SourceContext extends ParserRuleContext {
		public TerminalNode EOF() { return getToken(CLIFParser.EOF, 0); }
		public List<DirectiveContext> directive() {
			return getRuleContexts(DirectiveContext.class);
		}
		public DirectiveContext directive(int i) {
			return getRuleContext(DirectiveContext.class,i);
		}
		public List<InclusionContext> inclusion() {
			return getRuleContexts(InclusionContext.class);
		}
		public InclusionContext inclusion(int i) {
			return getRuleContext(InclusionContext.class,i);
		}
		public List<StructContext> struct() {
			return getRuleContexts(StructContext.class);
		}
		public StructContext struct(int i) {
			return getRuleContext(StructContext.class,i);
		}
		public List<UnionContext> union() {
			return getRuleContexts(UnionContext.class);
		}
		public UnionContext union(int i) {
			return getRuleContext(UnionContext.class,i);
		}
		public List<ModelContext> model() {
			return getRuleContexts(ModelContext.class);
		}
		public ModelContext model(int i) {
			return getRuleContext(ModelContext.class,i);
		}
		public List<KissContext> kiss() {
			return getRuleContexts(KissContext.class);
		}
		public KissContext kiss(int i) {
			return getRuleContext(KissContext.class,i);
		}
		public SourceContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_source; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof CLIFParserVisitor ) return ((CLIFParserVisitor<? extends T>)visitor).visitSource(this);
			else return visitor.visitChildren(this);
		}
	}

	public final SourceContext source() throws RecognitionException {
		SourceContext _localctx = new SourceContext(_ctx, getState());
		enterRule(_localctx, 0, RULE_source);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(51);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==AT) {
				{
				{
				setState(48);
				directive();
				}
				}
				setState(53);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(57);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==INCLUDE) {
				{
				{
				setState(54);
				inclusion();
				}
				}
				setState(59);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(66);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (((_la) & ~0x3f) == 0 && ((1L << _la) & 60L) != 0) {
				{
				setState(64);
				_errHandler.sync(this);
				switch (_input.LA(1)) {
				case STRUCT:
					{
					setState(60);
					struct();
					}
					break;
				case UNION:
					{
					setState(61);
					union();
					}
					break;
				case MODEL:
					{
					setState(62);
					model();
					}
					break;
				case KISS:
					{
					setState(63);
					kiss();
					}
					break;
				default:
					throw new NoViableAltException(this);
				}
				}
				setState(68);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(69);
			match(EOF);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class DirectiveContext extends ParserRuleContext {
		public TerminalNode AT() { return getToken(CLIFParser.AT, 0); }
		public TerminalNode NAME() { return getToken(CLIFParser.NAME, 0); }
		public DirectiveContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_directive; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof CLIFParserVisitor ) return ((CLIFParserVisitor<? extends T>)visitor).visitDirective(this);
			else return visitor.visitChildren(this);
		}
	}

	public final DirectiveContext directive() throws RecognitionException {
		DirectiveContext _localctx = new DirectiveContext(_ctx, getState());
		enterRule(_localctx, 2, RULE_directive);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(71);
			match(AT);
			setState(72);
			match(NAME);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class InclusionContext extends ParserRuleContext {
		public TerminalNode INCLUDE() { return getToken(CLIFParser.INCLUDE, 0); }
		public TerminalNode FILEPATH() { return getToken(CLIFParser.FILEPATH, 0); }
		public TerminalNode FP_SEMIC() { return getToken(CLIFParser.FP_SEMIC, 0); }
		public InclusionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_inclusion; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof CLIFParserVisitor ) return ((CLIFParserVisitor<? extends T>)visitor).visitInclusion(this);
			else return visitor.visitChildren(this);
		}
	}

	public final InclusionContext inclusion() throws RecognitionException {
		InclusionContext _localctx = new InclusionContext(_ctx, getState());
		enterRule(_localctx, 4, RULE_inclusion);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(74);
			match(INCLUDE);
			setState(75);
			match(FILEPATH);
			setState(76);
			match(FP_SEMIC);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class NumContext extends ParserRuleContext {
		public TerminalNode DECIM() { return getToken(CLIFParser.DECIM, 0); }
		public TerminalNode HEXAD() { return getToken(CLIFParser.HEXAD, 0); }
		public TerminalNode OCTAL() { return getToken(CLIFParser.OCTAL, 0); }
		public TerminalNode BINAR() { return getToken(CLIFParser.BINAR, 0); }
		public NumContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_num; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof CLIFParserVisitor ) return ((CLIFParserVisitor<? extends T>)visitor).visitNum(this);
			else return visitor.visitChildren(this);
		}
	}

	public final NumContext num() throws RecognitionException {
		NumContext _localctx = new NumContext(_ctx, getState());
		enterRule(_localctx, 6, RULE_num);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(78);
			_la = _input.LA(1);
			if ( !(((_la) & ~0x3f) == 0 && ((1L << _la) & 4222124650659840L) != 0) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class VarlistContext extends ParserRuleContext {
		public List<VarContext> var() {
			return getRuleContexts(VarContext.class);
		}
		public VarContext var(int i) {
			return getRuleContext(VarContext.class,i);
		}
		public List<TerminalNode> COMMA() { return getTokens(CLIFParser.COMMA); }
		public TerminalNode COMMA(int i) {
			return getToken(CLIFParser.COMMA, i);
		}
		public VarlistContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_varlist; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof CLIFParserVisitor ) return ((CLIFParserVisitor<? extends T>)visitor).visitVarlist(this);
			else return visitor.visitChildren(this);
		}
	}

	public final VarlistContext varlist() throws RecognitionException {
		VarlistContext _localctx = new VarlistContext(_ctx, getState());
		enterRule(_localctx, 8, RULE_varlist);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(80);
			var();
			setState(85);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==COMMA) {
				{
				{
				setState(81);
				match(COMMA);
				setState(82);
				var();
				}
				}
				setState(87);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class VarContext extends ParserRuleContext {
		public Token type;
		public Token name;
		public List<TerminalNode> NAME() { return getTokens(CLIFParser.NAME); }
		public TerminalNode NAME(int i) {
			return getToken(CLIFParser.NAME, i);
		}
		public TerminalNode RAW() { return getToken(CLIFParser.RAW, 0); }
		public VarContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_var; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof CLIFParserVisitor ) return ((CLIFParserVisitor<? extends T>)visitor).visitVar(this);
			else return visitor.visitChildren(this);
		}
	}

	public final VarContext var() throws RecognitionException {
		VarContext _localctx = new VarContext(_ctx, getState());
		enterRule(_localctx, 10, RULE_var);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(89);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,5,_ctx) ) {
			case 1:
				{
				setState(88);
				((VarContext)_localctx).type = match(NAME);
				}
				break;
			}
			setState(92);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==RAW) {
				{
				setState(91);
				match(RAW);
				}
			}

			setState(94);
			((VarContext)_localctx).name = match(NAME);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class RefContext extends ParserRuleContext {
		public MemberContext member() {
			return getRuleContext(MemberContext.class,0);
		}
		public SliceContext slice() {
			return getRuleContext(SliceContext.class,0);
		}
		public RefContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_ref; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof CLIFParserVisitor ) return ((CLIFParserVisitor<? extends T>)visitor).visitRef(this);
			else return visitor.visitChildren(this);
		}
	}

	public final RefContext ref() throws RecognitionException {
		RefContext _localctx = new RefContext(_ctx, getState());
		enterRule(_localctx, 12, RULE_ref);
		try {
			setState(98);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,7,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(96);
				member();
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(97);
				slice();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class MemberContext extends ParserRuleContext {
		public List<TerminalNode> NAME() { return getTokens(CLIFParser.NAME); }
		public TerminalNode NAME(int i) {
			return getToken(CLIFParser.NAME, i);
		}
		public List<TerminalNode> DOT() { return getTokens(CLIFParser.DOT); }
		public TerminalNode DOT(int i) {
			return getToken(CLIFParser.DOT, i);
		}
		public MemberContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_member; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof CLIFParserVisitor ) return ((CLIFParserVisitor<? extends T>)visitor).visitMember(this);
			else return visitor.visitChildren(this);
		}
	}

	public final MemberContext member() throws RecognitionException {
		MemberContext _localctx = new MemberContext(_ctx, getState());
		enterRule(_localctx, 14, RULE_member);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(100);
			match(NAME);
			setState(105);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,8,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					{
					{
					setState(101);
					match(DOT);
					setState(102);
					match(NAME);
					}
					} 
				}
				setState(107);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,8,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class SliceContext extends ParserRuleContext {
		public NumContext a;
		public NumContext b;
		public MemberContext member() {
			return getRuleContext(MemberContext.class,0);
		}
		public TerminalNode LBRAK() { return getToken(CLIFParser.LBRAK, 0); }
		public TerminalNode RBRAK() { return getToken(CLIFParser.RBRAK, 0); }
		public List<NumContext> num() {
			return getRuleContexts(NumContext.class);
		}
		public NumContext num(int i) {
			return getRuleContext(NumContext.class,i);
		}
		public TerminalNode COLON() { return getToken(CLIFParser.COLON, 0); }
		public SliceContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_slice; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof CLIFParserVisitor ) return ((CLIFParserVisitor<? extends T>)visitor).visitSlice(this);
			else return visitor.visitChildren(this);
		}
	}

	public final SliceContext slice() throws RecognitionException {
		SliceContext _localctx = new SliceContext(_ctx, getState());
		enterRule(_localctx, 16, RULE_slice);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(108);
			member();
			setState(109);
			match(LBRAK);
			setState(110);
			((SliceContext)_localctx).a = num();
			setState(113);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==COLON) {
				{
				setState(111);
				match(COLON);
				setState(112);
				((SliceContext)_localctx).b = num();
				}
			}

			setState(115);
			match(RBRAK);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class DeclareContext extends ParserRuleContext {
		public StructContext struct() {
			return getRuleContext(StructContext.class,0);
		}
		public UnionContext union() {
			return getRuleContext(UnionContext.class,0);
		}
		public VarContext var() {
			return getRuleContext(VarContext.class,0);
		}
		public TerminalNode SEMIC() { return getToken(CLIFParser.SEMIC, 0); }
		public DeclareContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_declare; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof CLIFParserVisitor ) return ((CLIFParserVisitor<? extends T>)visitor).visitDeclare(this);
			else return visitor.visitChildren(this);
		}
	}

	public final DeclareContext declare() throws RecognitionException {
		DeclareContext _localctx = new DeclareContext(_ctx, getState());
		enterRule(_localctx, 18, RULE_declare);
		try {
			setState(122);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case STRUCT:
				enterOuterAlt(_localctx, 1);
				{
				setState(117);
				struct();
				}
				break;
			case UNION:
				enterOuterAlt(_localctx, 2);
				{
				setState(118);
				union();
				}
				break;
			case NAME:
			case RAW:
				enterOuterAlt(_localctx, 3);
				{
				setState(119);
				var();
				setState(120);
				match(SEMIC);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class StructContext extends ParserRuleContext {
		public TerminalNode STRUCT() { return getToken(CLIFParser.STRUCT, 0); }
		public TerminalNode NAME() { return getToken(CLIFParser.NAME, 0); }
		public TerminalNode RBRACE() { return getToken(CLIFParser.RBRACE, 0); }
		public TerminalNode LBRACE() { return getToken(CLIFParser.LBRACE, 0); }
		public TerminalNode RAW() { return getToken(CLIFParser.RAW, 0); }
		public List<DeclareContext> declare() {
			return getRuleContexts(DeclareContext.class);
		}
		public DeclareContext declare(int i) {
			return getRuleContext(DeclareContext.class,i);
		}
		public StructContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_struct; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof CLIFParserVisitor ) return ((CLIFParserVisitor<? extends T>)visitor).visitStruct(this);
			else return visitor.visitChildren(this);
		}
	}

	public final StructContext struct() throws RecognitionException {
		StructContext _localctx = new StructContext(_ctx, getState());
		enterRule(_localctx, 20, RULE_struct);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(124);
			match(STRUCT);
			setState(126);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==RAW) {
				{
				setState(125);
				match(RAW);
				}
			}

			setState(128);
			match(NAME);
			setState(129);
			match(RBRACE);
			setState(131); 
			_errHandler.sync(this);
			_la = _input.LA(1);
			do {
				{
				{
				setState(130);
				declare();
				}
				}
				setState(133); 
				_errHandler.sync(this);
				_la = _input.LA(1);
			} while ( ((_la) & ~0x3f) == 0 && ((1L << _la) & 4644337115725836L) != 0 );
			setState(135);
			match(LBRACE);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class UnionContext extends ParserRuleContext {
		public TerminalNode UNION() { return getToken(CLIFParser.UNION, 0); }
		public TerminalNode NAME() { return getToken(CLIFParser.NAME, 0); }
		public TerminalNode RBRACE() { return getToken(CLIFParser.RBRACE, 0); }
		public TerminalNode LBRACE() { return getToken(CLIFParser.LBRACE, 0); }
		public TerminalNode RAW() { return getToken(CLIFParser.RAW, 0); }
		public List<DeclareContext> declare() {
			return getRuleContexts(DeclareContext.class);
		}
		public DeclareContext declare(int i) {
			return getRuleContext(DeclareContext.class,i);
		}
		public UnionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_union; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof CLIFParserVisitor ) return ((CLIFParserVisitor<? extends T>)visitor).visitUnion(this);
			else return visitor.visitChildren(this);
		}
	}

	public final UnionContext union() throws RecognitionException {
		UnionContext _localctx = new UnionContext(_ctx, getState());
		enterRule(_localctx, 22, RULE_union);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(137);
			match(UNION);
			setState(139);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==RAW) {
				{
				setState(138);
				match(RAW);
				}
			}

			setState(141);
			match(NAME);
			setState(142);
			match(RBRACE);
			setState(144); 
			_errHandler.sync(this);
			_la = _input.LA(1);
			do {
				{
				{
				setState(143);
				declare();
				}
				}
				setState(146); 
				_errHandler.sync(this);
				_la = _input.LA(1);
			} while ( ((_la) & ~0x3f) == 0 && ((1L << _la) & 4644337115725836L) != 0 );
			setState(148);
			match(LBRACE);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class ModelContext extends ParserRuleContext {
		public VarlistContext inputs;
		public VarlistContext outputs;
		public TerminalNode MODEL() { return getToken(CLIFParser.MODEL, 0); }
		public TerminalNode NAME() { return getToken(CLIFParser.NAME, 0); }
		public TerminalNode LPAR() { return getToken(CLIFParser.LPAR, 0); }
		public TerminalNode RPAR() { return getToken(CLIFParser.RPAR, 0); }
		public Model_bodyContext model_body() {
			return getRuleContext(Model_bodyContext.class,0);
		}
		public TerminalNode ARROW() { return getToken(CLIFParser.ARROW, 0); }
		public List<VarlistContext> varlist() {
			return getRuleContexts(VarlistContext.class);
		}
		public VarlistContext varlist(int i) {
			return getRuleContext(VarlistContext.class,i);
		}
		public ModelContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_model; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof CLIFParserVisitor ) return ((CLIFParserVisitor<? extends T>)visitor).visitModel(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ModelContext model() throws RecognitionException {
		ModelContext _localctx = new ModelContext(_ctx, getState());
		enterRule(_localctx, 24, RULE_model);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(150);
			match(MODEL);
			setState(151);
			match(NAME);
			setState(152);
			match(LPAR);
			setState(154);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==NAME || _la==RAW) {
				{
				setState(153);
				((ModelContext)_localctx).inputs = varlist();
				}
			}

			setState(156);
			match(RPAR);
			setState(159);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==ARROW) {
				{
				setState(157);
				match(ARROW);
				setState(158);
				((ModelContext)_localctx).outputs = varlist();
				}
			}

			setState(161);
			model_body();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class Model_bodyContext extends ParserRuleContext {
		public TerminalNode LBRACE() { return getToken(CLIFParser.LBRACE, 0); }
		public TerminalNode RBRACE() { return getToken(CLIFParser.RBRACE, 0); }
		public List<TerminalNode> SEMIC() { return getTokens(CLIFParser.SEMIC); }
		public TerminalNode SEMIC(int i) {
			return getToken(CLIFParser.SEMIC, i);
		}
		public List<Model_stmtContext> model_stmt() {
			return getRuleContexts(Model_stmtContext.class);
		}
		public Model_stmtContext model_stmt(int i) {
			return getRuleContext(Model_stmtContext.class,i);
		}
		public Model_bodyContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_model_body; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof CLIFParserVisitor ) return ((CLIFParserVisitor<? extends T>)visitor).visitModel_body(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Model_bodyContext model_body() throws RecognitionException {
		Model_bodyContext _localctx = new Model_bodyContext(_ctx, getState());
		enterRule(_localctx, 26, RULE_model_body);
		int _la;
		try {
			int _alt;
			setState(182);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case LBRACE:
				enterOuterAlt(_localctx, 1);
				{
				setState(163);
				match(LBRACE);
				setState(170);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (((_la) & ~0x3f) == 0 && ((1L << _la) & 140737521912128L) != 0) {
					{
					{
					setState(165);
					_errHandler.sync(this);
					_la = _input.LA(1);
					if (((_la) & ~0x3f) == 0 && ((1L << _la) & 140737488357696L) != 0) {
						{
						setState(164);
						model_stmt();
						}
					}

					setState(167);
					match(SEMIC);
					}
					}
					setState(172);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				setState(173);
				match(RBRACE);
				}
				break;
			case STATIC:
			case IF:
			case BREAK:
			case SEMIC:
			case NAME:
				enterOuterAlt(_localctx, 2);
				{
				setState(178); 
				_errHandler.sync(this);
				_alt = 1;
				do {
					switch (_alt) {
					case 1:
						{
						{
						setState(175);
						_errHandler.sync(this);
						_la = _input.LA(1);
						if (((_la) & ~0x3f) == 0 && ((1L << _la) & 140737488357696L) != 0) {
							{
							setState(174);
							model_stmt();
							}
						}

						setState(177);
						match(SEMIC);
						}
						}
						break;
					default:
						throw new NoViableAltException(this);
					}
					setState(180); 
					_errHandler.sync(this);
					_alt = getInterpreter().adaptivePredict(_input,20,_ctx);
				} while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER );
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class Model_stmtContext extends ParserRuleContext {
		public Model_stmtContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_model_stmt; }
	 
		public Model_stmtContext() { }
		public void copyFrom(Model_stmtContext ctx) {
			super.copyFrom(ctx);
		}
	}
	@SuppressWarnings("CheckReturnValue")
	public static class ModBreakContext extends Model_stmtContext {
		public TerminalNode BREAK() { return getToken(CLIFParser.BREAK, 0); }
		public ModBreakContext(Model_stmtContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof CLIFParserVisitor ) return ((CLIFParserVisitor<? extends T>)visitor).visitModBreak(this);
			else return visitor.visitChildren(this);
		}
	}
	@SuppressWarnings("CheckReturnValue")
	public static class ModLatchContext extends Model_stmtContext {
		public TerminalNode STATIC() { return getToken(CLIFParser.STATIC, 0); }
		public TerminalNode RAW() { return getToken(CLIFParser.RAW, 0); }
		public VarContext var() {
			return getRuleContext(VarContext.class,0);
		}
		public TerminalNode SET() { return getToken(CLIFParser.SET, 0); }
		public ExpContext exp() {
			return getRuleContext(ExpContext.class,0);
		}
		public ModLatchContext(Model_stmtContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof CLIFParserVisitor ) return ((CLIFParserVisitor<? extends T>)visitor).visitModLatch(this);
			else return visitor.visitChildren(this);
		}
	}
	@SuppressWarnings("CheckReturnValue")
	public static class ModAssignContext extends Model_stmtContext {
		public MemberContext member() {
			return getRuleContext(MemberContext.class,0);
		}
		public TerminalNode SET() { return getToken(CLIFParser.SET, 0); }
		public ExpContext exp() {
			return getRuleContext(ExpContext.class,0);
		}
		public ModAssignContext(Model_stmtContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof CLIFParserVisitor ) return ((CLIFParserVisitor<? extends T>)visitor).visitModAssign(this);
			else return visitor.visitChildren(this);
		}
	}
	@SuppressWarnings("CheckReturnValue")
	public static class ModSubcktContext extends Model_stmtContext {
		public Token nam;
		public Token mod;
		public TerminalNode SET() { return getToken(CLIFParser.SET, 0); }
		public TerminalNode NEW() { return getToken(CLIFParser.NEW, 0); }
		public TerminalNode LPAR() { return getToken(CLIFParser.LPAR, 0); }
		public TerminalNode RPAR() { return getToken(CLIFParser.RPAR, 0); }
		public List<TerminalNode> NAME() { return getTokens(CLIFParser.NAME); }
		public TerminalNode NAME(int i) {
			return getToken(CLIFParser.NAME, i);
		}
		public ArgsContext args() {
			return getRuleContext(ArgsContext.class,0);
		}
		public ModSubcktContext(Model_stmtContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof CLIFParserVisitor ) return ((CLIFParserVisitor<? extends T>)visitor).visitModSubckt(this);
			else return visitor.visitChildren(this);
		}
	}
	@SuppressWarnings("CheckReturnValue")
	public static class ModIfContext extends Model_stmtContext {
		public Model_ifContext model_if() {
			return getRuleContext(Model_ifContext.class,0);
		}
		public ModIfContext(Model_stmtContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof CLIFParserVisitor ) return ((CLIFParserVisitor<? extends T>)visitor).visitModIf(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Model_stmtContext model_stmt() throws RecognitionException {
		Model_stmtContext _localctx = new Model_stmtContext(_ctx, getState());
		enterRule(_localctx, 28, RULE_model_stmt);
		int _la;
		try {
			setState(206);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,24,_ctx) ) {
			case 1:
				_localctx = new ModAssignContext(_localctx);
				enterOuterAlt(_localctx, 1);
				{
				setState(184);
				member();
				setState(185);
				match(SET);
				setState(186);
				exp(0);
				}
				break;
			case 2:
				_localctx = new ModSubcktContext(_localctx);
				enterOuterAlt(_localctx, 2);
				{
				setState(188);
				((ModSubcktContext)_localctx).nam = match(NAME);
				setState(189);
				match(SET);
				setState(190);
				match(NEW);
				setState(191);
				((ModSubcktContext)_localctx).mod = match(NAME);
				setState(192);
				match(LPAR);
				setState(194);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (((_la) & ~0x3f) == 0 && ((1L << _la) & 4362863212838912L) != 0) {
					{
					setState(193);
					args();
					}
				}

				setState(196);
				match(RPAR);
				}
				break;
			case 3:
				_localctx = new ModLatchContext(_localctx);
				enterOuterAlt(_localctx, 3);
				{
				setState(197);
				match(STATIC);
				setState(198);
				match(RAW);
				setState(199);
				var();
				setState(202);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==SET) {
					{
					setState(200);
					match(SET);
					setState(201);
					exp(0);
					}
				}

				}
				break;
			case 4:
				_localctx = new ModIfContext(_localctx);
				enterOuterAlt(_localctx, 4);
				{
				setState(204);
				model_if();
				}
				break;
			case 5:
				_localctx = new ModBreakContext(_localctx);
				enterOuterAlt(_localctx, 5);
				{
				setState(205);
				match(BREAK);
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class Model_ifContext extends ParserRuleContext {
		public ExpContext exp;
		public List<ExpContext> cond = new ArrayList<ExpContext>();
		public Model_bodyContext model_body;
		public List<Model_bodyContext> blk = new ArrayList<Model_bodyContext>();
		public List<TerminalNode> IF() { return getTokens(CLIFParser.IF); }
		public TerminalNode IF(int i) {
			return getToken(CLIFParser.IF, i);
		}
		public List<TerminalNode> LPAR() { return getTokens(CLIFParser.LPAR); }
		public TerminalNode LPAR(int i) {
			return getToken(CLIFParser.LPAR, i);
		}
		public List<TerminalNode> RPAR() { return getTokens(CLIFParser.RPAR); }
		public TerminalNode RPAR(int i) {
			return getToken(CLIFParser.RPAR, i);
		}
		public List<ExpContext> exp() {
			return getRuleContexts(ExpContext.class);
		}
		public ExpContext exp(int i) {
			return getRuleContext(ExpContext.class,i);
		}
		public List<Model_bodyContext> model_body() {
			return getRuleContexts(Model_bodyContext.class);
		}
		public Model_bodyContext model_body(int i) {
			return getRuleContext(Model_bodyContext.class,i);
		}
		public List<TerminalNode> ELSE() { return getTokens(CLIFParser.ELSE); }
		public TerminalNode ELSE(int i) {
			return getToken(CLIFParser.ELSE, i);
		}
		public Model_ifContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_model_if; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof CLIFParserVisitor ) return ((CLIFParserVisitor<? extends T>)visitor).visitModel_if(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Model_ifContext model_if() throws RecognitionException {
		Model_ifContext _localctx = new Model_ifContext(_ctx, getState());
		enterRule(_localctx, 30, RULE_model_if);
		int _la;
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(208);
			match(IF);
			setState(209);
			match(LPAR);
			setState(210);
			((Model_ifContext)_localctx).exp = exp(0);
			((Model_ifContext)_localctx).cond.add(((Model_ifContext)_localctx).exp);
			setState(211);
			match(RPAR);
			setState(212);
			((Model_ifContext)_localctx).model_body = model_body();
			((Model_ifContext)_localctx).blk.add(((Model_ifContext)_localctx).model_body);
			setState(222);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,25,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					{
					{
					setState(213);
					match(ELSE);
					setState(214);
					match(IF);
					setState(215);
					match(LPAR);
					setState(216);
					((Model_ifContext)_localctx).exp = exp(0);
					((Model_ifContext)_localctx).cond.add(((Model_ifContext)_localctx).exp);
					setState(217);
					match(RPAR);
					setState(218);
					((Model_ifContext)_localctx).model_body = model_body();
					((Model_ifContext)_localctx).blk.add(((Model_ifContext)_localctx).model_body);
					}
					} 
				}
				setState(224);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,25,_ctx);
			}
			setState(227);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==ELSE) {
				{
				setState(225);
				match(ELSE);
				setState(226);
				((Model_ifContext)_localctx).model_body = model_body();
				((Model_ifContext)_localctx).blk.add(((Model_ifContext)_localctx).model_body);
				}
			}

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class ArgsContext extends ParserRuleContext {
		public ArglistContext arglist() {
			return getRuleContext(ArglistContext.class,0);
		}
		public ArgmapContext argmap() {
			return getRuleContext(ArgmapContext.class,0);
		}
		public TerminalNode COMMA() { return getToken(CLIFParser.COMMA, 0); }
		public ArgsContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_args; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof CLIFParserVisitor ) return ((CLIFParserVisitor<? extends T>)visitor).visitArgs(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ArgsContext args() throws RecognitionException {
		ArgsContext _localctx = new ArgsContext(_ctx, getState());
		enterRule(_localctx, 32, RULE_args);
		try {
			setState(235);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,27,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(229);
				arglist();
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(230);
				argmap();
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(231);
				arglist();
				setState(232);
				match(COMMA);
				setState(233);
				argmap();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class ArglistContext extends ParserRuleContext {
		public List<ExpContext> exp() {
			return getRuleContexts(ExpContext.class);
		}
		public ExpContext exp(int i) {
			return getRuleContext(ExpContext.class,i);
		}
		public List<TerminalNode> COMMA() { return getTokens(CLIFParser.COMMA); }
		public TerminalNode COMMA(int i) {
			return getToken(CLIFParser.COMMA, i);
		}
		public ArglistContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_arglist; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof CLIFParserVisitor ) return ((CLIFParserVisitor<? extends T>)visitor).visitArglist(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ArglistContext arglist() throws RecognitionException {
		ArglistContext _localctx = new ArglistContext(_ctx, getState());
		enterRule(_localctx, 34, RULE_arglist);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(237);
			exp(0);
			setState(242);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,28,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					{
					{
					setState(238);
					match(COMMA);
					setState(239);
					exp(0);
					}
					} 
				}
				setState(244);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,28,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class ArgmapContext extends ParserRuleContext {
		public List<ArgsetContext> argset() {
			return getRuleContexts(ArgsetContext.class);
		}
		public ArgsetContext argset(int i) {
			return getRuleContext(ArgsetContext.class,i);
		}
		public List<TerminalNode> COMMA() { return getTokens(CLIFParser.COMMA); }
		public TerminalNode COMMA(int i) {
			return getToken(CLIFParser.COMMA, i);
		}
		public ArgmapContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_argmap; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof CLIFParserVisitor ) return ((CLIFParserVisitor<? extends T>)visitor).visitArgmap(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ArgmapContext argmap() throws RecognitionException {
		ArgmapContext _localctx = new ArgmapContext(_ctx, getState());
		enterRule(_localctx, 36, RULE_argmap);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(245);
			argset();
			setState(250);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==COMMA) {
				{
				{
				setState(246);
				match(COMMA);
				setState(247);
				argset();
				}
				}
				setState(252);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class ArgsetContext extends ParserRuleContext {
		public TerminalNode NAME() { return getToken(CLIFParser.NAME, 0); }
		public TerminalNode SET() { return getToken(CLIFParser.SET, 0); }
		public ExpContext exp() {
			return getRuleContext(ExpContext.class,0);
		}
		public ArgsetContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_argset; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof CLIFParserVisitor ) return ((CLIFParserVisitor<? extends T>)visitor).visitArgset(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ArgsetContext argset() throws RecognitionException {
		ArgsetContext _localctx = new ArgsetContext(_ctx, getState());
		enterRule(_localctx, 38, RULE_argset);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(253);
			match(NAME);
			setState(254);
			match(SET);
			setState(255);
			exp(0);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class ExpContext extends ParserRuleContext {
		public ExpContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_exp; }
	 
		public ExpContext() { }
		public void copyFrom(ExpContext ctx) {
			super.copyFrom(ctx);
		}
	}
	@SuppressWarnings("CheckReturnValue")
	public static class ExpNumberContext extends ExpContext {
		public NumContext num() {
			return getRuleContext(NumContext.class,0);
		}
		public ExpNumberContext(ExpContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof CLIFParserVisitor ) return ((CLIFParserVisitor<? extends T>)visitor).visitExpNumber(this);
			else return visitor.visitChildren(this);
		}
	}
	@SuppressWarnings("CheckReturnValue")
	public static class ExpUnequalContext extends ExpContext {
		public Token op;
		public List<ExpContext> exp() {
			return getRuleContexts(ExpContext.class);
		}
		public ExpContext exp(int i) {
			return getRuleContext(ExpContext.class,i);
		}
		public TerminalNode LESS() { return getToken(CLIFParser.LESS, 0); }
		public TerminalNode LEQ() { return getToken(CLIFParser.LEQ, 0); }
		public TerminalNode GEQ() { return getToken(CLIFParser.GEQ, 0); }
		public TerminalNode GREAT() { return getToken(CLIFParser.GREAT, 0); }
		public ExpUnequalContext(ExpContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof CLIFParserVisitor ) return ((CLIFParserVisitor<? extends T>)visitor).visitExpUnequal(this);
			else return visitor.visitChildren(this);
		}
	}
	@SuppressWarnings("CheckReturnValue")
	public static class ExpBoolContext extends ExpContext {
		public TerminalNode BOOL() { return getToken(CLIFParser.BOOL, 0); }
		public ExpBoolContext(ExpContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof CLIFParserVisitor ) return ((CLIFParserVisitor<? extends T>)visitor).visitExpBool(this);
			else return visitor.visitChildren(this);
		}
	}
	@SuppressWarnings("CheckReturnValue")
	public static class ExpSumContext extends ExpContext {
		public Token op;
		public List<ExpContext> exp() {
			return getRuleContexts(ExpContext.class);
		}
		public ExpContext exp(int i) {
			return getRuleContext(ExpContext.class,i);
		}
		public TerminalNode PLUS() { return getToken(CLIFParser.PLUS, 0); }
		public TerminalNode MINUS() { return getToken(CLIFParser.MINUS, 0); }
		public ExpSumContext(ExpContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof CLIFParserVisitor ) return ((CLIFParserVisitor<? extends T>)visitor).visitExpSum(this);
			else return visitor.visitChildren(this);
		}
	}
	@SuppressWarnings("CheckReturnValue")
	public static class ExpShiftContext extends ExpContext {
		public Token op;
		public List<ExpContext> exp() {
			return getRuleContexts(ExpContext.class);
		}
		public ExpContext exp(int i) {
			return getRuleContext(ExpContext.class,i);
		}
		public TerminalNode LSHIFT() { return getToken(CLIFParser.LSHIFT, 0); }
		public TerminalNode RSHIFT() { return getToken(CLIFParser.RSHIFT, 0); }
		public ExpShiftContext(ExpContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof CLIFParserVisitor ) return ((CLIFParserVisitor<? extends T>)visitor).visitExpShift(this);
			else return visitor.visitChildren(this);
		}
	}
	@SuppressWarnings("CheckReturnValue")
	public static class ExpOrContext extends ExpContext {
		public Token op;
		public List<ExpContext> exp() {
			return getRuleContexts(ExpContext.class);
		}
		public ExpContext exp(int i) {
			return getRuleContext(ExpContext.class,i);
		}
		public TerminalNode OR() { return getToken(CLIFParser.OR, 0); }
		public ExpOrContext(ExpContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof CLIFParserVisitor ) return ((CLIFParserVisitor<? extends T>)visitor).visitExpOr(this);
			else return visitor.visitChildren(this);
		}
	}
	@SuppressWarnings("CheckReturnValue")
	public static class ExpMulContext extends ExpContext {
		public Token op;
		public List<ExpContext> exp() {
			return getRuleContexts(ExpContext.class);
		}
		public ExpContext exp(int i) {
			return getRuleContext(ExpContext.class,i);
		}
		public TerminalNode AST() { return getToken(CLIFParser.AST, 0); }
		public TerminalNode DIV() { return getToken(CLIFParser.DIV, 0); }
		public TerminalNode MOD() { return getToken(CLIFParser.MOD, 0); }
		public ExpMulContext(ExpContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof CLIFParserVisitor ) return ((CLIFParserVisitor<? extends T>)visitor).visitExpMul(this);
			else return visitor.visitChildren(this);
		}
	}
	@SuppressWarnings("CheckReturnValue")
	public static class ExpParensContext extends ExpContext {
		public TerminalNode LPAR() { return getToken(CLIFParser.LPAR, 0); }
		public ExpContext exp() {
			return getRuleContext(ExpContext.class,0);
		}
		public TerminalNode RPAR() { return getToken(CLIFParser.RPAR, 0); }
		public ExpParensContext(ExpContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof CLIFParserVisitor ) return ((CLIFParserVisitor<? extends T>)visitor).visitExpParens(this);
			else return visitor.visitChildren(this);
		}
	}
	@SuppressWarnings("CheckReturnValue")
	public static class ExpAndContext extends ExpContext {
		public Token op;
		public List<ExpContext> exp() {
			return getRuleContexts(ExpContext.class);
		}
		public ExpContext exp(int i) {
			return getRuleContext(ExpContext.class,i);
		}
		public TerminalNode AND() { return getToken(CLIFParser.AND, 0); }
		public ExpAndContext(ExpContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof CLIFParserVisitor ) return ((CLIFParserVisitor<? extends T>)visitor).visitExpAnd(this);
			else return visitor.visitChildren(this);
		}
	}
	@SuppressWarnings("CheckReturnValue")
	public static class ExpEqualContext extends ExpContext {
		public Token op;
		public List<ExpContext> exp() {
			return getRuleContexts(ExpContext.class);
		}
		public ExpContext exp(int i) {
			return getRuleContext(ExpContext.class,i);
		}
		public TerminalNode EQU() { return getToken(CLIFParser.EQU, 0); }
		public TerminalNode NEQ() { return getToken(CLIFParser.NEQ, 0); }
		public ExpEqualContext(ExpContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof CLIFParserVisitor ) return ((CLIFParserVisitor<? extends T>)visitor).visitExpEqual(this);
			else return visitor.visitChildren(this);
		}
	}
	@SuppressWarnings("CheckReturnValue")
	public static class ExpXorContext extends ExpContext {
		public Token op;
		public List<ExpContext> exp() {
			return getRuleContexts(ExpContext.class);
		}
		public ExpContext exp(int i) {
			return getRuleContext(ExpContext.class,i);
		}
		public TerminalNode XOR() { return getToken(CLIFParser.XOR, 0); }
		public ExpXorContext(ExpContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof CLIFParserVisitor ) return ((CLIFParserVisitor<? extends T>)visitor).visitExpXor(this);
			else return visitor.visitChildren(this);
		}
	}
	@SuppressWarnings("CheckReturnValue")
	public static class ExpConcatContext extends ExpContext {
		public Token op;
		public List<ExpContext> exp() {
			return getRuleContexts(ExpContext.class);
		}
		public ExpContext exp(int i) {
			return getRuleContext(ExpContext.class,i);
		}
		public TerminalNode CONCAT() { return getToken(CLIFParser.CONCAT, 0); }
		public ExpConcatContext(ExpContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof CLIFParserVisitor ) return ((CLIFParserVisitor<? extends T>)visitor).visitExpConcat(this);
			else return visitor.visitChildren(this);
		}
	}
	@SuppressWarnings("CheckReturnValue")
	public static class ExpMuxContext extends ExpContext {
		public List<ExpContext> exp() {
			return getRuleContexts(ExpContext.class);
		}
		public ExpContext exp(int i) {
			return getRuleContext(ExpContext.class,i);
		}
		public TerminalNode QUEST() { return getToken(CLIFParser.QUEST, 0); }
		public TerminalNode COLON() { return getToken(CLIFParser.COLON, 0); }
		public ExpMuxContext(ExpContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof CLIFParserVisitor ) return ((CLIFParserVisitor<? extends T>)visitor).visitExpMux(this);
			else return visitor.visitChildren(this);
		}
	}
	@SuppressWarnings("CheckReturnValue")
	public static class ExpRefContext extends ExpContext {
		public RefContext ref() {
			return getRuleContext(RefContext.class,0);
		}
		public ExpRefContext(ExpContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof CLIFParserVisitor ) return ((CLIFParserVisitor<? extends T>)visitor).visitExpRef(this);
			else return visitor.visitChildren(this);
		}
	}
	@SuppressWarnings("CheckReturnValue")
	public static class ExpNotContext extends ExpContext {
		public TerminalNode NOT() { return getToken(CLIFParser.NOT, 0); }
		public ExpContext exp() {
			return getRuleContext(ExpContext.class,0);
		}
		public ExpNotContext(ExpContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof CLIFParserVisitor ) return ((CLIFParserVisitor<? extends T>)visitor).visitExpNot(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ExpContext exp() throws RecognitionException {
		return exp(0);
	}

	private ExpContext exp(int _p) throws RecognitionException {
		ParserRuleContext _parentctx = _ctx;
		int _parentState = getState();
		ExpContext _localctx = new ExpContext(_ctx, _parentState);
		ExpContext _prevctx = _localctx;
		int _startState = 40;
		enterRecursionRule(_localctx, 40, RULE_exp, _p);
		int _la;
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(267);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case HEXAD:
			case OCTAL:
			case BINAR:
			case DECIM:
				{
				_localctx = new ExpNumberContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;

				setState(258);
				num();
				}
				break;
			case BOOL:
				{
				_localctx = new ExpBoolContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(259);
				match(BOOL);
				}
				break;
			case NAME:
				{
				_localctx = new ExpRefContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(260);
				ref();
				}
				break;
			case LPAR:
				{
				_localctx = new ExpParensContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(261);
				match(LPAR);
				setState(262);
				exp(0);
				setState(263);
				match(RPAR);
				}
				break;
			case NOT:
				{
				_localctx = new ExpNotContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(265);
				match(NOT);
				setState(266);
				exp(11);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
			_ctx.stop = _input.LT(-1);
			setState(304);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,32,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					if ( _parseListeners!=null ) triggerExitRuleEvent();
					_prevctx = _localctx;
					{
					setState(302);
					_errHandler.sync(this);
					switch ( getInterpreter().adaptivePredict(_input,31,_ctx) ) {
					case 1:
						{
						_localctx = new ExpMulContext(new ExpContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_exp);
						setState(269);
						if (!(precpred(_ctx, 10))) throw new FailedPredicateException(this, "precpred(_ctx, 10)");
						setState(270);
						((ExpMulContext)_localctx).op = _input.LT(1);
						_la = _input.LA(1);
						if ( !(((_la) & ~0x3f) == 0 && ((1L << _la) & 123145302310912L) != 0) ) {
							((ExpMulContext)_localctx).op = (Token)_errHandler.recoverInline(this);
						}
						else {
							if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
							_errHandler.reportMatch(this);
							consume();
						}
						setState(271);
						exp(11);
						}
						break;
					case 2:
						{
						_localctx = new ExpSumContext(new ExpContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_exp);
						setState(272);
						if (!(precpred(_ctx, 9))) throw new FailedPredicateException(this, "precpred(_ctx, 9)");
						setState(273);
						((ExpSumContext)_localctx).op = _input.LT(1);
						_la = _input.LA(1);
						if ( !(_la==PLUS || _la==MINUS) ) {
							((ExpSumContext)_localctx).op = (Token)_errHandler.recoverInline(this);
						}
						else {
							if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
							_errHandler.reportMatch(this);
							consume();
						}
						setState(274);
						exp(10);
						}
						break;
					case 3:
						{
						_localctx = new ExpConcatContext(new ExpContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_exp);
						setState(275);
						if (!(precpred(_ctx, 8))) throw new FailedPredicateException(this, "precpred(_ctx, 8)");
						setState(276);
						((ExpConcatContext)_localctx).op = match(CONCAT);
						setState(277);
						exp(8);
						}
						break;
					case 4:
						{
						_localctx = new ExpShiftContext(new ExpContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_exp);
						setState(278);
						if (!(precpred(_ctx, 7))) throw new FailedPredicateException(this, "precpred(_ctx, 7)");
						setState(279);
						((ExpShiftContext)_localctx).op = _input.LT(1);
						_la = _input.LA(1);
						if ( !(_la==LSHIFT || _la==RSHIFT) ) {
							((ExpShiftContext)_localctx).op = (Token)_errHandler.recoverInline(this);
						}
						else {
							if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
							_errHandler.reportMatch(this);
							consume();
						}
						setState(280);
						exp(8);
						}
						break;
					case 5:
						{
						_localctx = new ExpUnequalContext(new ExpContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_exp);
						setState(281);
						if (!(precpred(_ctx, 6))) throw new FailedPredicateException(this, "precpred(_ctx, 6)");
						setState(282);
						((ExpUnequalContext)_localctx).op = _input.LT(1);
						_la = _input.LA(1);
						if ( !(((_la) & ~0x3f) == 0 && ((1L << _la) & 3504693313536L) != 0) ) {
							((ExpUnequalContext)_localctx).op = (Token)_errHandler.recoverInline(this);
						}
						else {
							if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
							_errHandler.reportMatch(this);
							consume();
						}
						setState(283);
						exp(7);
						}
						break;
					case 6:
						{
						_localctx = new ExpEqualContext(new ExpContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_exp);
						setState(284);
						if (!(precpred(_ctx, 5))) throw new FailedPredicateException(this, "precpred(_ctx, 5)");
						setState(285);
						((ExpEqualContext)_localctx).op = _input.LT(1);
						_la = _input.LA(1);
						if ( !(_la==EQU || _la==NEQ) ) {
							((ExpEqualContext)_localctx).op = (Token)_errHandler.recoverInline(this);
						}
						else {
							if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
							_errHandler.reportMatch(this);
							consume();
						}
						setState(286);
						exp(6);
						}
						break;
					case 7:
						{
						_localctx = new ExpAndContext(new ExpContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_exp);
						setState(287);
						if (!(precpred(_ctx, 4))) throw new FailedPredicateException(this, "precpred(_ctx, 4)");
						setState(288);
						((ExpAndContext)_localctx).op = match(AND);
						setState(289);
						exp(5);
						}
						break;
					case 8:
						{
						_localctx = new ExpXorContext(new ExpContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_exp);
						setState(290);
						if (!(precpred(_ctx, 3))) throw new FailedPredicateException(this, "precpred(_ctx, 3)");
						setState(291);
						((ExpXorContext)_localctx).op = match(XOR);
						setState(292);
						exp(4);
						}
						break;
					case 9:
						{
						_localctx = new ExpOrContext(new ExpContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_exp);
						setState(293);
						if (!(precpred(_ctx, 2))) throw new FailedPredicateException(this, "precpred(_ctx, 2)");
						setState(294);
						((ExpOrContext)_localctx).op = match(OR);
						setState(295);
						exp(3);
						}
						break;
					case 10:
						{
						_localctx = new ExpMuxContext(new ExpContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_exp);
						setState(296);
						if (!(precpred(_ctx, 1))) throw new FailedPredicateException(this, "precpred(_ctx, 1)");
						setState(297);
						match(QUEST);
						setState(298);
						exp(0);
						setState(299);
						match(COLON);
						setState(300);
						exp(1);
						}
						break;
					}
					} 
				}
				setState(306);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,32,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			unrollRecursionContexts(_parentctx);
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class KissContext extends ParserRuleContext {
		public VarlistContext inputs;
		public VarlistContext outputs;
		public Kiss_bodyContext body;
		public TerminalNode KISS() { return getToken(CLIFParser.KISS, 0); }
		public TerminalNode NAME() { return getToken(CLIFParser.NAME, 0); }
		public TerminalNode LPAR() { return getToken(CLIFParser.LPAR, 0); }
		public TerminalNode RPAR() { return getToken(CLIFParser.RPAR, 0); }
		public Kiss_bodyContext kiss_body() {
			return getRuleContext(Kiss_bodyContext.class,0);
		}
		public TerminalNode ARROW() { return getToken(CLIFParser.ARROW, 0); }
		public List<VarlistContext> varlist() {
			return getRuleContexts(VarlistContext.class);
		}
		public VarlistContext varlist(int i) {
			return getRuleContext(VarlistContext.class,i);
		}
		public KissContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_kiss; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof CLIFParserVisitor ) return ((CLIFParserVisitor<? extends T>)visitor).visitKiss(this);
			else return visitor.visitChildren(this);
		}
	}

	public final KissContext kiss() throws RecognitionException {
		KissContext _localctx = new KissContext(_ctx, getState());
		enterRule(_localctx, 42, RULE_kiss);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(307);
			match(KISS);
			setState(308);
			match(NAME);
			setState(309);
			match(LPAR);
			setState(311);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==NAME || _la==RAW) {
				{
				setState(310);
				((KissContext)_localctx).inputs = varlist();
				}
			}

			setState(313);
			match(RPAR);
			setState(316);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==ARROW) {
				{
				setState(314);
				match(ARROW);
				setState(315);
				((KissContext)_localctx).outputs = varlist();
				}
			}

			setState(318);
			((KissContext)_localctx).body = kiss_body();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class Kiss_bodyContext extends ParserRuleContext {
		public TerminalNode LBRACE() { return getToken(CLIFParser.LBRACE, 0); }
		public TerminalNode RBRACE() { return getToken(CLIFParser.RBRACE, 0); }
		public List<TerminalNode> SEMIC() { return getTokens(CLIFParser.SEMIC); }
		public TerminalNode SEMIC(int i) {
			return getToken(CLIFParser.SEMIC, i);
		}
		public List<Kiss_stmtContext> kiss_stmt() {
			return getRuleContexts(Kiss_stmtContext.class);
		}
		public Kiss_stmtContext kiss_stmt(int i) {
			return getRuleContext(Kiss_stmtContext.class,i);
		}
		public Kiss_bodyContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_kiss_body; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof CLIFParserVisitor ) return ((CLIFParserVisitor<? extends T>)visitor).visitKiss_body(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Kiss_bodyContext kiss_body() throws RecognitionException {
		Kiss_bodyContext _localctx = new Kiss_bodyContext(_ctx, getState());
		enterRule(_localctx, 44, RULE_kiss_body);
		int _la;
		try {
			setState(339);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case LBRACE:
				enterOuterAlt(_localctx, 1);
				{
				setState(320);
				match(LBRACE);
				setState(327);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (((_la) & ~0x3f) == 0 && ((1L << _la) & 33557504L) != 0) {
					{
					{
					setState(322);
					_errHandler.sync(this);
					_la = _input.LA(1);
					if (_la==CASE || _la==BREAK) {
						{
						setState(321);
						kiss_stmt();
						}
					}

					setState(324);
					match(SEMIC);
					}
					}
					setState(329);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				setState(330);
				match(RBRACE);
				}
				break;
			case CASE:
			case BREAK:
			case SEMIC:
				enterOuterAlt(_localctx, 2);
				{
				setState(335); 
				_errHandler.sync(this);
				_la = _input.LA(1);
				do {
					{
					{
					setState(332);
					_errHandler.sync(this);
					_la = _input.LA(1);
					if (_la==CASE || _la==BREAK) {
						{
						setState(331);
						kiss_stmt();
						}
					}

					setState(334);
					match(SEMIC);
					}
					}
					setState(337); 
					_errHandler.sync(this);
					_la = _input.LA(1);
				} while ( ((_la) & ~0x3f) == 0 && ((1L << _la) & 33557504L) != 0 );
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class Kiss_stmtContext extends ParserRuleContext {
		public Kiss_stmtContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_kiss_stmt; }
	 
		public Kiss_stmtContext() { }
		public void copyFrom(Kiss_stmtContext ctx) {
			super.copyFrom(ctx);
		}
	}
	@SuppressWarnings("CheckReturnValue")
	public static class KissCaseContext extends Kiss_stmtContext {
		public TerminalNode CASE() { return getToken(CLIFParser.CASE, 0); }
		public TerminalNode NAME() { return getToken(CLIFParser.NAME, 0); }
		public TerminalNode COLON() { return getToken(CLIFParser.COLON, 0); }
		public KissCaseContext(Kiss_stmtContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof CLIFParserVisitor ) return ((CLIFParserVisitor<? extends T>)visitor).visitKissCase(this);
			else return visitor.visitChildren(this);
		}
	}
	@SuppressWarnings("CheckReturnValue")
	public static class KissBreakContext extends Kiss_stmtContext {
		public TerminalNode BREAK() { return getToken(CLIFParser.BREAK, 0); }
		public TerminalNode NAME() { return getToken(CLIFParser.NAME, 0); }
		public KissBreakContext(Kiss_stmtContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof CLIFParserVisitor ) return ((CLIFParserVisitor<? extends T>)visitor).visitKissBreak(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Kiss_stmtContext kiss_stmt() throws RecognitionException {
		Kiss_stmtContext _localctx = new Kiss_stmtContext(_ctx, getState());
		enterRule(_localctx, 46, RULE_kiss_stmt);
		int _la;
		try {
			setState(348);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case CASE:
				_localctx = new KissCaseContext(_localctx);
				enterOuterAlt(_localctx, 1);
				{
				setState(341);
				match(CASE);
				setState(342);
				match(NAME);
				setState(343);
				match(COLON);
				}
				break;
			case BREAK:
				_localctx = new KissBreakContext(_localctx);
				enterOuterAlt(_localctx, 2);
				{
				setState(344);
				match(BREAK);
				setState(346);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==NAME) {
					{
					setState(345);
					match(NAME);
					}
				}

				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public boolean sempred(RuleContext _localctx, int ruleIndex, int predIndex) {
		switch (ruleIndex) {
		case 20:
			return exp_sempred((ExpContext)_localctx, predIndex);
		}
		return true;
	}
	private boolean exp_sempred(ExpContext _localctx, int predIndex) {
		switch (predIndex) {
		case 0:
			return precpred(_ctx, 10);
		case 1:
			return precpred(_ctx, 9);
		case 2:
			return precpred(_ctx, 8);
		case 3:
			return precpred(_ctx, 7);
		case 4:
			return precpred(_ctx, 6);
		case 5:
			return precpred(_ctx, 5);
		case 6:
			return precpred(_ctx, 4);
		case 7:
			return precpred(_ctx, 3);
		case 8:
			return precpred(_ctx, 2);
		case 9:
			return precpred(_ctx, 1);
		}
		return true;
	}

	public static final String _serializedATN =
		"\u0004\u0001<\u015f\u0002\u0000\u0007\u0000\u0002\u0001\u0007\u0001\u0002"+
		"\u0002\u0007\u0002\u0002\u0003\u0007\u0003\u0002\u0004\u0007\u0004\u0002"+
		"\u0005\u0007\u0005\u0002\u0006\u0007\u0006\u0002\u0007\u0007\u0007\u0002"+
		"\b\u0007\b\u0002\t\u0007\t\u0002\n\u0007\n\u0002\u000b\u0007\u000b\u0002"+
		"\f\u0007\f\u0002\r\u0007\r\u0002\u000e\u0007\u000e\u0002\u000f\u0007\u000f"+
		"\u0002\u0010\u0007\u0010\u0002\u0011\u0007\u0011\u0002\u0012\u0007\u0012"+
		"\u0002\u0013\u0007\u0013\u0002\u0014\u0007\u0014\u0002\u0015\u0007\u0015"+
		"\u0002\u0016\u0007\u0016\u0002\u0017\u0007\u0017\u0001\u0000\u0005\u0000"+
		"2\b\u0000\n\u0000\f\u00005\t\u0000\u0001\u0000\u0005\u00008\b\u0000\n"+
		"\u0000\f\u0000;\t\u0000\u0001\u0000\u0001\u0000\u0001\u0000\u0001\u0000"+
		"\u0005\u0000A\b\u0000\n\u0000\f\u0000D\t\u0000\u0001\u0000\u0001\u0000"+
		"\u0001\u0001\u0001\u0001\u0001\u0001\u0001\u0002\u0001\u0002\u0001\u0002"+
		"\u0001\u0002\u0001\u0003\u0001\u0003\u0001\u0004\u0001\u0004\u0001\u0004"+
		"\u0005\u0004T\b\u0004\n\u0004\f\u0004W\t\u0004\u0001\u0005\u0003\u0005"+
		"Z\b\u0005\u0001\u0005\u0003\u0005]\b\u0005\u0001\u0005\u0001\u0005\u0001"+
		"\u0006\u0001\u0006\u0003\u0006c\b\u0006\u0001\u0007\u0001\u0007\u0001"+
		"\u0007\u0005\u0007h\b\u0007\n\u0007\f\u0007k\t\u0007\u0001\b\u0001\b\u0001"+
		"\b\u0001\b\u0001\b\u0003\br\b\b\u0001\b\u0001\b\u0001\t\u0001\t\u0001"+
		"\t\u0001\t\u0001\t\u0003\t{\b\t\u0001\n\u0001\n\u0003\n\u007f\b\n\u0001"+
		"\n\u0001\n\u0001\n\u0004\n\u0084\b\n\u000b\n\f\n\u0085\u0001\n\u0001\n"+
		"\u0001\u000b\u0001\u000b\u0003\u000b\u008c\b\u000b\u0001\u000b\u0001\u000b"+
		"\u0001\u000b\u0004\u000b\u0091\b\u000b\u000b\u000b\f\u000b\u0092\u0001"+
		"\u000b\u0001\u000b\u0001\f\u0001\f\u0001\f\u0001\f\u0003\f\u009b\b\f\u0001"+
		"\f\u0001\f\u0001\f\u0003\f\u00a0\b\f\u0001\f\u0001\f\u0001\r\u0001\r\u0003"+
		"\r\u00a6\b\r\u0001\r\u0005\r\u00a9\b\r\n\r\f\r\u00ac\t\r\u0001\r\u0001"+
		"\r\u0003\r\u00b0\b\r\u0001\r\u0004\r\u00b3\b\r\u000b\r\f\r\u00b4\u0003"+
		"\r\u00b7\b\r\u0001\u000e\u0001\u000e\u0001\u000e\u0001\u000e\u0001\u000e"+
		"\u0001\u000e\u0001\u000e\u0001\u000e\u0001\u000e\u0001\u000e\u0003\u000e"+
		"\u00c3\b\u000e\u0001\u000e\u0001\u000e\u0001\u000e\u0001\u000e\u0001\u000e"+
		"\u0001\u000e\u0003\u000e\u00cb\b\u000e\u0001\u000e\u0001\u000e\u0003\u000e"+
		"\u00cf\b\u000e\u0001\u000f\u0001\u000f\u0001\u000f\u0001\u000f\u0001\u000f"+
		"\u0001\u000f\u0001\u000f\u0001\u000f\u0001\u000f\u0001\u000f\u0001\u000f"+
		"\u0001\u000f\u0005\u000f\u00dd\b\u000f\n\u000f\f\u000f\u00e0\t\u000f\u0001"+
		"\u000f\u0001\u000f\u0003\u000f\u00e4\b\u000f\u0001\u0010\u0001\u0010\u0001"+
		"\u0010\u0001\u0010\u0001\u0010\u0001\u0010\u0003\u0010\u00ec\b\u0010\u0001"+
		"\u0011\u0001\u0011\u0001\u0011\u0005\u0011\u00f1\b\u0011\n\u0011\f\u0011"+
		"\u00f4\t\u0011\u0001\u0012\u0001\u0012\u0001\u0012\u0005\u0012\u00f9\b"+
		"\u0012\n\u0012\f\u0012\u00fc\t\u0012\u0001\u0013\u0001\u0013\u0001\u0013"+
		"\u0001\u0013\u0001\u0014\u0001\u0014\u0001\u0014\u0001\u0014\u0001\u0014"+
		"\u0001\u0014\u0001\u0014\u0001\u0014\u0001\u0014\u0001\u0014\u0003\u0014"+
		"\u010c\b\u0014\u0001\u0014\u0001\u0014\u0001\u0014\u0001\u0014\u0001\u0014"+
		"\u0001\u0014\u0001\u0014\u0001\u0014\u0001\u0014\u0001\u0014\u0001\u0014"+
		"\u0001\u0014\u0001\u0014\u0001\u0014\u0001\u0014\u0001\u0014\u0001\u0014"+
		"\u0001\u0014\u0001\u0014\u0001\u0014\u0001\u0014\u0001\u0014\u0001\u0014"+
		"\u0001\u0014\u0001\u0014\u0001\u0014\u0001\u0014\u0001\u0014\u0001\u0014"+
		"\u0001\u0014\u0001\u0014\u0001\u0014\u0001\u0014\u0005\u0014\u012f\b\u0014"+
		"\n\u0014\f\u0014\u0132\t\u0014\u0001\u0015\u0001\u0015\u0001\u0015\u0001"+
		"\u0015\u0003\u0015\u0138\b\u0015\u0001\u0015\u0001\u0015\u0001\u0015\u0003"+
		"\u0015\u013d\b\u0015\u0001\u0015\u0001\u0015\u0001\u0016\u0001\u0016\u0003"+
		"\u0016\u0143\b\u0016\u0001\u0016\u0005\u0016\u0146\b\u0016\n\u0016\f\u0016"+
		"\u0149\t\u0016\u0001\u0016\u0001\u0016\u0003\u0016\u014d\b\u0016\u0001"+
		"\u0016\u0004\u0016\u0150\b\u0016\u000b\u0016\f\u0016\u0151\u0003\u0016"+
		"\u0154\b\u0016\u0001\u0017\u0001\u0017\u0001\u0017\u0001\u0017\u0001\u0017"+
		"\u0003\u0017\u015b\b\u0017\u0003\u0017\u015d\b\u0017\u0001\u0017\u0000"+
		"\u0001(\u0018\u0000\u0002\u0004\u0006\b\n\f\u000e\u0010\u0012\u0014\u0016"+
		"\u0018\u001a\u001c\u001e \"$&(*,.\u0000\u0006\u0001\u000003\u0001\u0000"+
		",.\u0001\u0000*+\u0001\u0000\"#\u0002\u0000$%()\u0001\u0000&\'\u0182\u0000"+
		"3\u0001\u0000\u0000\u0000\u0002G\u0001\u0000\u0000\u0000\u0004J\u0001"+
		"\u0000\u0000\u0000\u0006N\u0001\u0000\u0000\u0000\bP\u0001\u0000\u0000"+
		"\u0000\nY\u0001\u0000\u0000\u0000\fb\u0001\u0000\u0000\u0000\u000ed\u0001"+
		"\u0000\u0000\u0000\u0010l\u0001\u0000\u0000\u0000\u0012z\u0001\u0000\u0000"+
		"\u0000\u0014|\u0001\u0000\u0000\u0000\u0016\u0089\u0001\u0000\u0000\u0000"+
		"\u0018\u0096\u0001\u0000\u0000\u0000\u001a\u00b6\u0001\u0000\u0000\u0000"+
		"\u001c\u00ce\u0001\u0000\u0000\u0000\u001e\u00d0\u0001\u0000\u0000\u0000"+
		" \u00eb\u0001\u0000\u0000\u0000\"\u00ed\u0001\u0000\u0000\u0000$\u00f5"+
		"\u0001\u0000\u0000\u0000&\u00fd\u0001\u0000\u0000\u0000(\u010b\u0001\u0000"+
		"\u0000\u0000*\u0133\u0001\u0000\u0000\u0000,\u0153\u0001\u0000\u0000\u0000"+
		".\u015c\u0001\u0000\u0000\u000002\u0003\u0002\u0001\u000010\u0001\u0000"+
		"\u0000\u000025\u0001\u0000\u0000\u000031\u0001\u0000\u0000\u000034\u0001"+
		"\u0000\u0000\u000049\u0001\u0000\u0000\u000053\u0001\u0000\u0000\u0000"+
		"68\u0003\u0004\u0002\u000076\u0001\u0000\u0000\u00008;\u0001\u0000\u0000"+
		"\u000097\u0001\u0000\u0000\u00009:\u0001\u0000\u0000\u0000:B\u0001\u0000"+
		"\u0000\u0000;9\u0001\u0000\u0000\u0000<A\u0003\u0014\n\u0000=A\u0003\u0016"+
		"\u000b\u0000>A\u0003\u0018\f\u0000?A\u0003*\u0015\u0000@<\u0001\u0000"+
		"\u0000\u0000@=\u0001\u0000\u0000\u0000@>\u0001\u0000\u0000\u0000@?\u0001"+
		"\u0000\u0000\u0000AD\u0001\u0000\u0000\u0000B@\u0001\u0000\u0000\u0000"+
		"BC\u0001\u0000\u0000\u0000CE\u0001\u0000\u0000\u0000DB\u0001\u0000\u0000"+
		"\u0000EF\u0005\u0000\u0000\u0001F\u0001\u0001\u0000\u0000\u0000GH\u0005"+
		"\u000f\u0000\u0000HI\u0005/\u0000\u0000I\u0003\u0001\u0000\u0000\u0000"+
		"JK\u0005\u0001\u0000\u0000KL\u0005<\u0000\u0000LM\u0005;\u0000\u0000M"+
		"\u0005\u0001\u0000\u0000\u0000NO\u0007\u0000\u0000\u0000O\u0007\u0001"+
		"\u0000\u0000\u0000PU\u0003\n\u0005\u0000QR\u0005\u0018\u0000\u0000RT\u0003"+
		"\n\u0005\u0000SQ\u0001\u0000\u0000\u0000TW\u0001\u0000\u0000\u0000US\u0001"+
		"\u0000\u0000\u0000UV\u0001\u0000\u0000\u0000V\t\u0001\u0000\u0000\u0000"+
		"WU\u0001\u0000\u0000\u0000XZ\u0005/\u0000\u0000YX\u0001\u0000\u0000\u0000"+
		"YZ\u0001\u0000\u0000\u0000Z\\\u0001\u0000\u0000\u0000[]\u00054\u0000\u0000"+
		"\\[\u0001\u0000\u0000\u0000\\]\u0001\u0000\u0000\u0000]^\u0001\u0000\u0000"+
		"\u0000^_\u0005/\u0000\u0000_\u000b\u0001\u0000\u0000\u0000`c\u0003\u000e"+
		"\u0007\u0000ac\u0003\u0010\b\u0000b`\u0001\u0000\u0000\u0000ba\u0001\u0000"+
		"\u0000\u0000c\r\u0001\u0000\u0000\u0000di\u0005/\u0000\u0000ef\u0005\u001b"+
		"\u0000\u0000fh\u0005/\u0000\u0000ge\u0001\u0000\u0000\u0000hk\u0001\u0000"+
		"\u0000\u0000ig\u0001\u0000\u0000\u0000ij\u0001\u0000\u0000\u0000j\u000f"+
		"\u0001\u0000\u0000\u0000ki\u0001\u0000\u0000\u0000lm\u0003\u000e\u0007"+
		"\u0000mn\u0005\u0012\u0000\u0000nq\u0003\u0006\u0003\u0000op\u0005\u0017"+
		"\u0000\u0000pr\u0003\u0006\u0003\u0000qo\u0001\u0000\u0000\u0000qr\u0001"+
		"\u0000\u0000\u0000rs\u0001\u0000\u0000\u0000st\u0005\u0013\u0000\u0000"+
		"t\u0011\u0001\u0000\u0000\u0000u{\u0003\u0014\n\u0000v{\u0003\u0016\u000b"+
		"\u0000wx\u0003\n\u0005\u0000xy\u0005\u0019\u0000\u0000y{\u0001\u0000\u0000"+
		"\u0000zu\u0001\u0000\u0000\u0000zv\u0001\u0000\u0000\u0000zw\u0001\u0000"+
		"\u0000\u0000{\u0013\u0001\u0000\u0000\u0000|~\u0005\u0002\u0000\u0000"+
		"}\u007f\u00054\u0000\u0000~}\u0001\u0000\u0000\u0000~\u007f\u0001\u0000"+
		"\u0000\u0000\u007f\u0080\u0001\u0000\u0000\u0000\u0080\u0081\u0005/\u0000"+
		"\u0000\u0081\u0083\u0005\u0015\u0000\u0000\u0082\u0084\u0003\u0012\t\u0000"+
		"\u0083\u0082\u0001\u0000\u0000\u0000\u0084\u0085\u0001\u0000\u0000\u0000"+
		"\u0085\u0083\u0001\u0000\u0000\u0000\u0085\u0086\u0001\u0000\u0000\u0000"+
		"\u0086\u0087\u0001\u0000\u0000\u0000\u0087\u0088\u0005\u0014\u0000\u0000"+
		"\u0088\u0015\u0001\u0000\u0000\u0000\u0089\u008b\u0005\u0003\u0000\u0000"+
		"\u008a\u008c\u00054\u0000\u0000\u008b\u008a\u0001\u0000\u0000\u0000\u008b"+
		"\u008c\u0001\u0000\u0000\u0000\u008c\u008d\u0001\u0000\u0000\u0000\u008d"+
		"\u008e\u0005/\u0000\u0000\u008e\u0090\u0005\u0015\u0000\u0000\u008f\u0091"+
		"\u0003\u0012\t\u0000\u0090\u008f\u0001\u0000\u0000\u0000\u0091\u0092\u0001"+
		"\u0000\u0000\u0000\u0092\u0090\u0001\u0000\u0000\u0000\u0092\u0093\u0001"+
		"\u0000\u0000\u0000\u0093\u0094\u0001\u0000\u0000\u0000\u0094\u0095\u0005"+
		"\u0014\u0000\u0000\u0095\u0017\u0001\u0000\u0000\u0000\u0096\u0097\u0005"+
		"\u0004\u0000\u0000\u0097\u0098\u0005/\u0000\u0000\u0098\u009a\u0005\u0010"+
		"\u0000\u0000\u0099\u009b\u0003\b\u0004\u0000\u009a\u0099\u0001\u0000\u0000"+
		"\u0000\u009a\u009b\u0001\u0000\u0000\u0000\u009b\u009c\u0001\u0000\u0000"+
		"\u0000\u009c\u009f\u0005\u0011\u0000\u0000\u009d\u009e\u0005\u0016\u0000"+
		"\u0000\u009e\u00a0\u0003\b\u0004\u0000\u009f\u009d\u0001\u0000\u0000\u0000"+
		"\u009f\u00a0\u0001\u0000\u0000\u0000\u00a0\u00a1\u0001\u0000\u0000\u0000"+
		"\u00a1\u00a2\u0003\u001a\r\u0000\u00a2\u0019\u0001\u0000\u0000\u0000\u00a3"+
		"\u00aa\u0005\u0014\u0000\u0000\u00a4\u00a6\u0003\u001c\u000e\u0000\u00a5"+
		"\u00a4\u0001\u0000\u0000\u0000\u00a5\u00a6\u0001\u0000\u0000\u0000\u00a6"+
		"\u00a7\u0001\u0000\u0000\u0000\u00a7\u00a9\u0005\u0019\u0000\u0000\u00a8"+
		"\u00a5\u0001\u0000\u0000\u0000\u00a9\u00ac\u0001\u0000\u0000\u0000\u00aa"+
		"\u00a8\u0001\u0000\u0000\u0000\u00aa\u00ab\u0001\u0000\u0000\u0000\u00ab"+
		"\u00ad\u0001\u0000\u0000\u0000\u00ac\u00aa\u0001\u0000\u0000\u0000\u00ad"+
		"\u00b7\u0005\u0015\u0000\u0000\u00ae\u00b0\u0003\u001c\u000e\u0000\u00af"+
		"\u00ae\u0001\u0000\u0000\u0000\u00af\u00b0\u0001\u0000\u0000\u0000\u00b0"+
		"\u00b1\u0001\u0000\u0000\u0000\u00b1\u00b3\u0005\u0019\u0000\u0000\u00b2"+
		"\u00af\u0001\u0000\u0000\u0000\u00b3\u00b4\u0001\u0000\u0000\u0000\u00b4"+
		"\u00b2\u0001\u0000\u0000\u0000\u00b4\u00b5\u0001\u0000\u0000\u0000\u00b5"+
		"\u00b7\u0001\u0000\u0000\u0000\u00b6\u00a3\u0001\u0000\u0000\u0000\u00b6"+
		"\u00b2\u0001\u0000\u0000\u0000\u00b7\u001b\u0001\u0000\u0000\u0000\u00b8"+
		"\u00b9\u0003\u000e\u0007\u0000\u00b9\u00ba\u0005\u001c\u0000\u0000\u00ba"+
		"\u00bb\u0003(\u0014\u0000\u00bb\u00cf\u0001\u0000\u0000\u0000\u00bc\u00bd"+
		"\u0005/\u0000\u0000\u00bd\u00be\u0005\u001c\u0000\u0000\u00be\u00bf\u0005"+
		"\u0007\u0000\u0000\u00bf\u00c0\u0005/\u0000\u0000\u00c0\u00c2\u0005\u0010"+
		"\u0000\u0000\u00c1\u00c3\u0003 \u0010\u0000\u00c2\u00c1\u0001\u0000\u0000"+
		"\u0000\u00c2\u00c3\u0001\u0000\u0000\u0000\u00c3\u00c4\u0001\u0000\u0000"+
		"\u0000\u00c4\u00cf\u0005\u0011\u0000\u0000\u00c5\u00c6\u0005\u0006\u0000"+
		"\u0000\u00c6\u00c7\u00054\u0000\u0000\u00c7\u00ca\u0003\n\u0005\u0000"+
		"\u00c8\u00c9\u0005\u001c\u0000\u0000\u00c9\u00cb\u0003(\u0014\u0000\u00ca"+
		"\u00c8\u0001\u0000\u0000\u0000\u00ca\u00cb\u0001\u0000\u0000\u0000\u00cb"+
		"\u00cf\u0001\u0000\u0000\u0000\u00cc\u00cf\u0003\u001e\u000f\u0000\u00cd"+
		"\u00cf\u0005\u000b\u0000\u0000\u00ce\u00b8\u0001\u0000\u0000\u0000\u00ce"+
		"\u00bc\u0001\u0000\u0000\u0000\u00ce\u00c5\u0001\u0000\u0000\u0000\u00ce"+
		"\u00cc\u0001\u0000\u0000\u0000\u00ce\u00cd\u0001\u0000\u0000\u0000\u00cf"+
		"\u001d\u0001\u0000\u0000\u0000\u00d0\u00d1\u0005\b\u0000\u0000\u00d1\u00d2"+
		"\u0005\u0010\u0000\u0000\u00d2\u00d3\u0003(\u0014\u0000\u00d3\u00d4\u0005"+
		"\u0011\u0000\u0000\u00d4\u00de\u0003\u001a\r\u0000\u00d5\u00d6\u0005\t"+
		"\u0000\u0000\u00d6\u00d7\u0005\b\u0000\u0000\u00d7\u00d8\u0005\u0010\u0000"+
		"\u0000\u00d8\u00d9\u0003(\u0014\u0000\u00d9\u00da\u0005\u0011\u0000\u0000"+
		"\u00da\u00db\u0003\u001a\r\u0000\u00db\u00dd\u0001\u0000\u0000\u0000\u00dc"+
		"\u00d5\u0001\u0000\u0000\u0000\u00dd\u00e0\u0001\u0000\u0000\u0000\u00de"+
		"\u00dc\u0001\u0000\u0000\u0000\u00de\u00df\u0001\u0000\u0000\u0000\u00df"+
		"\u00e3\u0001\u0000\u0000\u0000\u00e0\u00de\u0001\u0000\u0000\u0000\u00e1"+
		"\u00e2\u0005\t\u0000\u0000\u00e2\u00e4\u0003\u001a\r\u0000\u00e3\u00e1"+
		"\u0001\u0000\u0000\u0000\u00e3\u00e4\u0001\u0000\u0000\u0000\u00e4\u001f"+
		"\u0001\u0000\u0000\u0000\u00e5\u00ec\u0003\"\u0011\u0000\u00e6\u00ec\u0003"+
		"$\u0012\u0000\u00e7\u00e8\u0003\"\u0011\u0000\u00e8\u00e9\u0005\u0018"+
		"\u0000\u0000\u00e9\u00ea\u0003$\u0012\u0000\u00ea\u00ec\u0001\u0000\u0000"+
		"\u0000\u00eb\u00e5\u0001\u0000\u0000\u0000\u00eb\u00e6\u0001\u0000\u0000"+
		"\u0000\u00eb\u00e7\u0001\u0000\u0000\u0000\u00ec!\u0001\u0000\u0000\u0000"+
		"\u00ed\u00f2\u0003(\u0014\u0000\u00ee\u00ef\u0005\u0018\u0000\u0000\u00ef"+
		"\u00f1\u0003(\u0014\u0000\u00f0\u00ee\u0001\u0000\u0000\u0000\u00f1\u00f4"+
		"\u0001\u0000\u0000\u0000\u00f2\u00f0\u0001\u0000\u0000\u0000\u00f2\u00f3"+
		"\u0001\u0000\u0000\u0000\u00f3#\u0001\u0000\u0000\u0000\u00f4\u00f2\u0001"+
		"\u0000\u0000\u0000\u00f5\u00fa\u0003&\u0013\u0000\u00f6\u00f7\u0005\u0018"+
		"\u0000\u0000\u00f7\u00f9\u0003&\u0013\u0000\u00f8\u00f6\u0001\u0000\u0000"+
		"\u0000\u00f9\u00fc\u0001\u0000\u0000\u0000\u00fa\u00f8\u0001\u0000\u0000"+
		"\u0000\u00fa\u00fb\u0001\u0000\u0000\u0000\u00fb%\u0001\u0000\u0000\u0000"+
		"\u00fc\u00fa\u0001\u0000\u0000\u0000\u00fd\u00fe\u0005/\u0000\u0000\u00fe"+
		"\u00ff\u0005\u001c\u0000\u0000\u00ff\u0100\u0003(\u0014\u0000\u0100\'"+
		"\u0001\u0000\u0000\u0000\u0101\u0102\u0006\u0014\uffff\uffff\u0000\u0102"+
		"\u010c\u0003\u0006\u0003\u0000\u0103\u010c\u0005\u000e\u0000\u0000\u0104"+
		"\u010c\u0003\f\u0006\u0000\u0105\u0106\u0005\u0010\u0000\u0000\u0106\u0107"+
		"\u0003(\u0014\u0000\u0107\u0108\u0005\u0011\u0000\u0000\u0108\u010c\u0001"+
		"\u0000\u0000\u0000\u0109\u010a\u0005\u001e\u0000\u0000\u010a\u010c\u0003"+
		"(\u0014\u000b\u010b\u0101\u0001\u0000\u0000\u0000\u010b\u0103\u0001\u0000"+
		"\u0000\u0000\u010b\u0104\u0001\u0000\u0000\u0000\u010b\u0105\u0001\u0000"+
		"\u0000\u0000\u010b\u0109\u0001\u0000\u0000\u0000\u010c\u0130\u0001\u0000"+
		"\u0000\u0000\u010d\u010e\n\n\u0000\u0000\u010e\u010f\u0007\u0001\u0000"+
		"\u0000\u010f\u012f\u0003(\u0014\u000b\u0110\u0111\n\t\u0000\u0000\u0111"+
		"\u0112\u0007\u0002\u0000\u0000\u0112\u012f\u0003(\u0014\n\u0113\u0114"+
		"\n\b\u0000\u0000\u0114\u0115\u0005\u001a\u0000\u0000\u0115\u012f\u0003"+
		"(\u0014\b\u0116\u0117\n\u0007\u0000\u0000\u0117\u0118\u0007\u0003\u0000"+
		"\u0000\u0118\u012f\u0003(\u0014\b\u0119\u011a\n\u0006\u0000\u0000\u011a"+
		"\u011b\u0007\u0004\u0000\u0000\u011b\u012f\u0003(\u0014\u0007\u011c\u011d"+
		"\n\u0005\u0000\u0000\u011d\u011e\u0007\u0005\u0000\u0000\u011e\u012f\u0003"+
		"(\u0014\u0006\u011f\u0120\n\u0004\u0000\u0000\u0120\u0121\u0005\u001f"+
		"\u0000\u0000\u0121\u012f\u0003(\u0014\u0005\u0122\u0123\n\u0003\u0000"+
		"\u0000\u0123\u0124\u0005!\u0000\u0000\u0124\u012f\u0003(\u0014\u0004\u0125"+
		"\u0126\n\u0002\u0000\u0000\u0126\u0127\u0005 \u0000\u0000\u0127\u012f"+
		"\u0003(\u0014\u0003\u0128\u0129\n\u0001\u0000\u0000\u0129\u012a\u0005"+
		"\u001d\u0000\u0000\u012a\u012b\u0003(\u0014\u0000\u012b\u012c\u0005\u0017"+
		"\u0000\u0000\u012c\u012d\u0003(\u0014\u0001\u012d\u012f\u0001\u0000\u0000"+
		"\u0000\u012e\u010d\u0001\u0000\u0000\u0000\u012e\u0110\u0001\u0000\u0000"+
		"\u0000\u012e\u0113\u0001\u0000\u0000\u0000\u012e\u0116\u0001\u0000\u0000"+
		"\u0000\u012e\u0119\u0001\u0000\u0000\u0000\u012e\u011c\u0001\u0000\u0000"+
		"\u0000\u012e\u011f\u0001\u0000\u0000\u0000\u012e\u0122\u0001\u0000\u0000"+
		"\u0000\u012e\u0125\u0001\u0000\u0000\u0000\u012e\u0128\u0001\u0000\u0000"+
		"\u0000\u012f\u0132\u0001\u0000\u0000\u0000\u0130\u012e\u0001\u0000\u0000"+
		"\u0000\u0130\u0131\u0001\u0000\u0000\u0000\u0131)\u0001\u0000\u0000\u0000"+
		"\u0132\u0130\u0001\u0000\u0000\u0000\u0133\u0134\u0005\u0005\u0000\u0000"+
		"\u0134\u0135\u0005/\u0000\u0000\u0135\u0137\u0005\u0010\u0000\u0000\u0136"+
		"\u0138\u0003\b\u0004\u0000\u0137\u0136\u0001\u0000\u0000\u0000\u0137\u0138"+
		"\u0001\u0000\u0000\u0000\u0138\u0139\u0001\u0000\u0000\u0000\u0139\u013c"+
		"\u0005\u0011\u0000\u0000\u013a\u013b\u0005\u0016\u0000\u0000\u013b\u013d"+
		"\u0003\b\u0004\u0000\u013c\u013a\u0001\u0000\u0000\u0000\u013c\u013d\u0001"+
		"\u0000\u0000\u0000\u013d\u013e\u0001\u0000\u0000\u0000\u013e\u013f\u0003"+
		",\u0016\u0000\u013f+\u0001\u0000\u0000\u0000\u0140\u0147\u0005\u0014\u0000"+
		"\u0000\u0141\u0143\u0003.\u0017\u0000\u0142\u0141\u0001\u0000\u0000\u0000"+
		"\u0142\u0143\u0001\u0000\u0000\u0000\u0143\u0144\u0001\u0000\u0000\u0000"+
		"\u0144\u0146\u0005\u0019\u0000\u0000\u0145\u0142\u0001\u0000\u0000\u0000"+
		"\u0146\u0149\u0001\u0000\u0000\u0000\u0147\u0145\u0001\u0000\u0000\u0000"+
		"\u0147\u0148\u0001\u0000\u0000\u0000\u0148\u014a\u0001\u0000\u0000\u0000"+
		"\u0149\u0147\u0001\u0000\u0000\u0000\u014a\u0154\u0005\u0015\u0000\u0000"+
		"\u014b\u014d\u0003.\u0017\u0000\u014c\u014b\u0001\u0000\u0000\u0000\u014c"+
		"\u014d\u0001\u0000\u0000\u0000\u014d\u014e\u0001\u0000\u0000\u0000\u014e"+
		"\u0150\u0005\u0019\u0000\u0000\u014f\u014c\u0001\u0000\u0000\u0000\u0150"+
		"\u0151\u0001\u0000\u0000\u0000\u0151\u014f\u0001\u0000\u0000\u0000\u0151"+
		"\u0152\u0001\u0000\u0000\u0000\u0152\u0154\u0001\u0000\u0000\u0000\u0153"+
		"\u0140\u0001\u0000\u0000\u0000\u0153\u014f\u0001\u0000\u0000\u0000\u0154"+
		"-\u0001\u0000\u0000\u0000\u0155\u0156\u0005\n\u0000\u0000\u0156\u0157"+
		"\u0005/\u0000\u0000\u0157\u015d\u0005\u0017\u0000\u0000\u0158\u015a\u0005"+
		"\u000b\u0000\u0000\u0159\u015b\u0005/\u0000\u0000\u015a\u0159\u0001\u0000"+
		"\u0000\u0000\u015a\u015b\u0001\u0000\u0000\u0000\u015b\u015d\u0001\u0000"+
		"\u0000\u0000\u015c\u0155\u0001\u0000\u0000\u0000\u015c\u0158\u0001\u0000"+
		"\u0000\u0000\u015d/\u0001\u0000\u0000\u0000*39@BUY\\biqz~\u0085\u008b"+
		"\u0092\u009a\u009f\u00a5\u00aa\u00af\u00b4\u00b6\u00c2\u00ca\u00ce\u00de"+
		"\u00e3\u00eb\u00f2\u00fa\u010b\u012e\u0130\u0137\u013c\u0142\u0147\u014c"+
		"\u0151\u0153\u015a\u015c";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}