import cool.micro.clif.SourceReader;

import static cool.micro.clif.BLIFWriter.*;


public class Main {
    public static void main(String[] args) {
        if (args.length < 1) {
            System.err.println("Missing main source argument.");
            System.exit(1);
        }

        try {
            var r = new SourceReader();
            r.readFile(args[0]);
            BLIFFileWriter.writeAllFiles(r.getMain(), "result");
        } catch (RuntimeException e) {
            System.out.flush();
            System.err.flush();
            System.exit(1);
        }
    }
}
