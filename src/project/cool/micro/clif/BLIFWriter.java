package cool.micro.clif;

import java.io.*;
import java.nio.file.*;
import java.util.Objects;
import java.util.stream.*;

import static cool.micro.clif.MyExceptions.*;
import static cool.micro.clif.SourceMapping.*;
import static cool.micro.clif.Utils.*;


public abstract class BLIFWriter<T extends Writer> implements Closeable {
    protected T writer;

    public static String getOutputSourceName(Source s) {
        return getOutputSourceName(s, null);
    }
    
    /** TODO use {@code requiring} and {@code targetdir} */
    public static String getOutputSourceName(Source s, Source requiring) {
        if (s.path == null)
            throw new NullPointerException("Source does not have a path.");
        String filename = s.path.getFileName().toString();
        int pos = filename.lastIndexOf(".");
        // If '.' is not the first or last character.
        if (pos > 0 && pos < (filename.length() - 1))
            filename = filename.substring(0, pos);
        return filename + ".blif";
    }
    
    
    @Override public void close() throws IOException {
        if (writer != null)
            writer.close();
    }
    
    @SuppressWarnings("unused")
    public abstract void openTargetStream(Source s);
    
    
    protected void write(String ...text)   {
        try {
            for (String t : text)
                writer.write(t);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
    public void writesp()                  { write(" ");                }
    public void writeln(String ...text)    { write(text);  write("\n"); }
    public void writeComment(String ...s)  { write("# ");  writeln(s);  }
    public void writeSameLine(int indent)  { write(" \\\n", " ".repeat(indent)); }
    public void writeSameLine()            { writeSameLine(4); }
    public void margin(Object a, Object b) {
        if (sizeof(a) > 0 && sizeof(b) > 0)
            writeln();
    }
    
    
    public String genGate(Target g, int bit) {
        return String.format(g.getSize() == 1 ? "%s" : "%s%d", g.getBLIFName(), bit);
    }
    public Stream<String> genGates(Target g) {
        return IntStream.range(g.getStart(), g.getEnd()).mapToObj(i -> genGate(g, i));
    }

    
    public void writeGateList(Target g) {
        Utils.interleave(genGates(g), this::write, this::writesp);
    }
    public void writeGateMap(boolean outward, Target a, Target b) {
        // will handle different type sizes
        var nullb = b == null;  // all gates will miss
        var s_end = genGates(Objects.requireNonNull(a)).iterator();
        var s_exp = nullb ? null : genGates(b).iterator();
        
        final boolean[] skip = {false};
        Utils.interleave(s_end, sa -> {
            String sb;
            if (!nullb && s_exp.hasNext())
                sb = s_exp.next();
            else if (!outward)
                sb = "0";  // input zero const
            else { // unused gate
                skip[0] = true;
                return;
            }
            write(sa, "=", sb);
        }, () -> {
            if (!skip[0])
                writesp();
            skip[0] = false;
        });
    }

    public void writeSource(Source s) {
        for (Source dep : s.includes)
            writeln(".search ", getOutputSourceName(dep, s));
        margin(s.includes, s.models);
        Utils.interleave(s.models.values(), this::writeModel, () -> { writeln(); writeln(); });
    }
    public void writeModel(Model m) {
        if (m.isExternal()) {
            writeComment("placeholder for model ", m.name);
            return;
        }
        
        writeln(".model   ", m.name);

        write(".inputs  ");
        Utils.interleave(m.getSinks(), this::writeGateList, () -> writeSameLine(9));
        writeln();

        write(".outputs ");
        Utils.interleave(m.getSources(), this::writeGateList, () -> writeSameLine(9));
        writeln();
        writeln();
        
        Utils.interleave(m.blocks, b -> {
            Objects.requireNonNull(b);
            if (b instanceof Const c)           writeConst(c);
            else if (b instanceof Subcircuit s) writeSubckt(s);
            else if (b instanceof Copy c)       writeCopy(c);
            else throw new NotImplementedError();
        }, this::writeln);

        writeln(".end");
    }
    
    public void writeConst(Const c) {
        writeComment("constant ", c.getName());
        for (int i = 0; i < c.getSize(); i++) {
            writeln(".names ", genGate(c, i));
            if ( (c.num & (1 << i)) != 0)
                writeln("1");
        }
    }
    public void writeSubckt(Subcircuit sub) {
        write(".subckt ", sub.model.name);
        writeSameLine();
        Utils.interleave(
            sub.getLinks().filter(e -> e.isSink() || !e.isUnused()),
            e -> {
                if (e instanceof Subcircuit.SourceEnd src) {
                    var me = src.optimized();
                    writeGateMap(true, src.ref, me != null ? me : src);
                } else if (e instanceof Subcircuit.SinkEnd snk)
                    writeGateMap(false, snk, snk.getLinked());
            },
            this::writeSameLine
        );
        writeln();
    }
    public void writeCopy(Copy c) {
        if (c.from instanceof Subcircuit.Endpoint<?> se)
            writeComment("copy ", se.getModel().getName(), ".", c.from.getName(), " to ", c.to.getName());
        else
            writeComment("copy ", c.from.getName(), " to ", c.to.getName());
        for (int i = 0; i < c.to.getSize(); i++) {
            writeln(".names ", genGate(c.from, i), " ", genGate(c.to, i));
            writeln("1 1");
        }
    }


    public static class BLIFStringWriter extends BLIFWriter<StringWriter> {
        @Override public void openTargetStream(Source s) {
            writer = new StringWriter();
        }

        public static StringWriter writeAllMerged(Source main) {
            var merged = new StringWriter();
            Utils.interleave(main.flatMap().iterator(), (space, s) -> {
                var outname = BLIFWriter.getOutputSourceName(s);
                try (var w = new BLIFStringWriter()) {
                    w.writer = merged;
                    if (space) {
                        w.writeln();
                        w.writeln();
                    }
                    w.writeComment(String.format("[[%s]]", outname));  // Utils.relpath(s.path)
                    w.writeSource(s);
                } catch (IOException e) {
                    throw new CloseException(e);
                }
            });
            return merged;
        }
    }


    public static class BLIFFileWriter extends BLIFWriter<BufferedWriter> {
        public final Path targetdir;

        public BLIFFileWriter(Path targetdir) {
            this.targetdir = targetdir;
        }

        @Override public void openTargetStream(Source s) {
            var outname = BLIFWriter.getOutputSourceName(s);
            FileWriter file;
            try {
                //noinspection ResultOfMethodCallIgnored
                new File(outname).getParentFile().mkdirs();  // mkdir -p
                file = new FileWriter(targetdir.resolve(outname).toString());
            } catch (IOException e) {
                throw new FileException(e);
            }
            writer = new BufferedWriter(file);
        }

        public static void writeAllFiles(Source main, String targetdir) {
            var maindir = main.path.getParent();
            var target = Path.of(targetdir);
            for (Source s : iter(main.flatMap())) {
                // s.path must be absolute, ignore non-canonical paths
                var rel = maindir.relativize(s.path);
                if (rel.startsWith("..")) {
                    System.err.printf(
                        "[warning] path is leaving target directory, skipped from compilation:\n    %s%n", s.path
                    );
                    continue;
                }
                var target2 = target.resolve(rel);
                try (var w = new BLIFFileWriter(target2)) {
                    w.openTargetStream(s);
                    w.writeSource(s);
                } catch (IOException e) {
                    throw new CloseException(e);
                }
            }
        }
    }
}
