package cool.micro.clif;

import java.nio.file.Path;
import java.util.*;
import java.util.function.Supplier;
import java.util.stream.Stream;

import lombok.*;

import static cool.micro.clif.Utils.*;
import static cool.micro.clif.MyExceptions.*;


public interface SourceMapping {
    class Source {
        public final Path path;
        public final LinkedHashSet<Source> includes = new LinkedHashSet<>();
        public final Namespace<Type> types = new Namespace<>(true, () -> includes.stream().map(s -> s.types));
        public final Namespace<Model> models = new Namespace<>(true, () -> includes.stream().map(s -> s.models));
        public RuntimeException lastError;
        public boolean mapped = false;

        public Source(Path p) { path = p; }

        public Stream<Source> flatMap() {
            return Stream.concat(
                Stream.of(this),
                this.includes.stream().flatMap(Source::flatMap)
            ).distinct();
        }
        public Type findType(String n) {
            return Type.find(this, n);
        }

        public boolean samePath(Source b) {
            return Objects.equals(path, b.path);
        }
        @Override public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            Source source = (Source) o;
            return samePath(source);
        }
        @Override public int hashCode() {
            return Objects.hash(path);
        }
    }

    
    interface Named {
        String getName();
        private static String desc(Named n) {
            return String.format("%s \"%s\"", clsName(n), n.getName());
        }
    }
    
    class MyMap<K,V> extends LinkedHashMap<K,V> {  // just to avoid `get(Object key)` signature
        public V lookup(K key) {
            return super.get(key);
        }
        @Deprecated(forRemoval=true)
        @Override public V get(Object key) {
            return super.get(key);
        }
    }
    
    @NoArgsConstructor @AllArgsConstructor
    class Namespace<T extends Named> extends MyMap<String, T> {
        private boolean noReplace = false;
        private Supplier<Stream<Namespace<T>>> fallback;
        public Namespace(boolean nrep) { noReplace = nrep; }
    
        @SuppressWarnings("UnusedReturnValue") public T put(T obj) {
            return this.put(obj.getName(), obj);
        }
        @SuppressWarnings("UnusedReturnValue") public T rawPut(String k, T obj) {
            return super.put(k, obj);
        }
        
        /** Can return {@code null} whether this instance and <b>all</b>
          * eventual fallback objects do not contain the specified {@code key}. */
        public T getOrFallback(String key) {
            var ret = super.lookup(key);
            if (ret != null || fallback == null)
                return ret;
            for (var ns : iter(fallback.get())) {
                var ret2 = ns.lookup(key);
                if (ret2 != null)
                    return ret2;
            }
            return null;
        }
        private void assertNotExisting(String key) {
            if (noReplace && super.containsKey(key))
                throw new RuntimeException(String.format("\"%s\" already exists.", key));
        }
    
        @Override public T put(String key, T value) {
            assertNotExisting(key);
            return super.put(key, value);
        }
        @Override public void putAll(Map<? extends String, ? extends T> m) {
            m.keySet().forEach(this::assertNotExisting);
            super.putAll(m);
        }
    }


    class Type implements Named {
        public String name;
        public int size;
        @Override public String getName() { return name; }
    
        private static final Namespace<Type> builtinTypesCache = new Namespace<>(true);
        static Type findBuiltin(String n) {
            final var name = (n == null || n.equals("int1") ? "bool" : n);
            final var ct = builtinTypesCache.lookup(name);
            if (ct != null)
                return ct;
            int size = 1;
            if (name.startsWith("int")) {
                size = Integer.parseInt(name.substring(3));
                if (size < 1)
                    throw new TypeError("Invalid type size.");
            } else if (!name.equals("bool"))
                return null;
            final Type t = new Type();
            t.name = name;
            t.size = size;
            builtinTypesCache.put(name, t);
            return t;
        }
        static Type findCustom(Source s, String name) {
            var t = s.types.lookup(name);
            if (t != null)
                return t;
            for (Source s2 : s.includes) {
                t = findCustom(s2, name);
                if (t != null)
                    return t;
            }
            return null;
        }
        static Type find(Source s, String n) {
            var t = Type.findBuiltin(n);
            if (t != null)
                return t;
            t = findCustom(s, n);
            if (t == null)
                throw new TypeError("Unknown type.");
            return t;
        }
    }
    
    /** BLIF code block */
    interface Block {}
    
    interface Target extends Named {
        int getEnd();
        default Type getType() { return null; }
        default int getStart() { return 0; }
        default int getSize()  { return getEnd() - getStart(); }
        default String getRaw()      { return null; }
        default String getBLIFName() { return Objects.requireNonNullElse(getRaw(), getName()); }
    }
    
    interface ReadableKind extends Target {
        Set<WritableKind> getReads();
        @SuppressWarnings("UnusedReturnValue")
        default boolean writeTo(WritableKind t2) {
            if (!getReads().add(t2))
                return false;
            t2.link(this);
            return true;
        }
        @SuppressWarnings("UnusedReturnValue")
        default boolean notWriteTo(WritableKind t2) {
            if (!getReads().remove(t2))
                return false;
            t2.link(null);
            return true;
        }
        default boolean isUnused() {
            return getReads().isEmpty();
        }
        default Model.Endpoint optimized() {
            var it = getReads().iterator();
            if (it.hasNext() && it.next() instanceof Model.Endpoint out && !it.hasNext())
                return out;
            return null;
        }
    }
    
    interface WritableKind {
        default void link(Target t) {
            var old = getLinked();
            if (old == t)
                return;
            if (old instanceof ReadableKind oldr)
                oldr.notWriteTo(this);
            ((Impl) this)._link(t);
            if (t instanceof ReadableKind newr)
                newr.writeTo(this);
        }
        Target getLinked();
        default boolean isUnused() { return getLinked() == null; }
        
        interface Impl extends WritableKind {
            void _link(Target t);
        }
    }
    
    abstract class TargetProxy<T extends Target> implements Target {
        public T ref;
        @Override public String getName() { return ref.getName(); }
        @Override public String getRaw()  { return ref.getRaw(); }
        @Override public int getStart()   { return ref.getStart(); }
        @Override public int getEnd()     { return ref.getEnd(); }
        @Override public Type getType()   { return ref.getType(); }
        @Override public String toString() {
            return String.format("%s of %s", clsName(this), ref.toString());
        }
    }
    
    abstract class TargetObj implements Target {
        public String name, raw;
        public Type type;
        @Override public String getName() { return name; }
        @Override public int getEnd()     { return type.size; }
        @Override public Type getType()   { return type; }
        @Override public String getRaw()  { return raw; }
        @Override public String toString() {
            return Named.desc(this);
        }
    }

    
    // TODO Member like Slice
    // TODO Struct <- Type, Named { members }
    
    class Slice extends TargetProxy<Target> {
        public int start, end;
        @Override public int getStart() { return start; }
        @Override public int getEnd()   { return end; }
        @Override public Type getType() { return null; }
    }
    
    // TODO Overlay <- Target { granular getName }
    
    
    @RequiredArgsConstructor(access=AccessLevel.PRIVATE)
    class Const implements Block, Target {
        public final int num;
        public final int size;
        public final String code;
        @Override public String getName() { return code; }
        @Override public int getEnd()     { return size; }
        @Override public String toString() {
            return Named.desc(this);
        }
    
        private static final Namespace<Const> constsCache;
        public static final Const ZERO;
        static {
            constsCache = new Namespace<>(true);
            ZERO = get("0");
        }
        static int getNumber(String num) {
            final var neg = num.charAt(0) == '-';
            final var canonic = num.toLowerCase().replaceAll("^-|_+", "");
            final int radix = switch (canonic.length() >= 2 ? canonic.substring(0, 2) : "") {
                case "0x" -> 16;
                case "0o" -> 8;
                case "0b" -> 2;
                default -> 10;
            };
            final String cont = (radix == 10) ? canonic : canonic.substring(2);
            return (neg ? -1 : 1) * Integer.parseInt(cont, radix);
        }
        static Const get(String code) {
            final var val = getNumber(code);
            final var cc = constsCache.lookup(val + "");
            if (cc != null)
                return cc;
            int minsize = sizeofnum(Math.abs(val)) + (val < 0 ? 1 : 0);
            // var t = Type.findBuiltin("int" + typesize);
            var cn = new Const(val, minsize, code);
            constsCache.put(val + "", cn);
            return cn;
        }
    }
    
    class Latch extends TargetObj {
        public final Target
            readTarget = new Target() {
                public final Latch parent = Latch.this;
                @Override public String getName() { return parent.name + ".r"; }
                @Override public int getEnd()     { return parent.getEnd(); }
            },
            writeTarget = new Target() {
                public final Latch parent = Latch.this;
                @Override public String getName() { return parent.name + ".w"; }
                @Override public int getEnd()     { return parent.getEnd(); }
            };
    }
    
    class Logic implements Block {
        public boolean onSet;
        // TODO I/O
        public List<String> config = new ArrayList<>();
    }
    
    
    interface EndpointKind extends Target {
        Model getModel();
        boolean isSource();  // from external perspective
        boolean isUnused();
        default boolean isSink() { return !isSource(); }
        private static String desc(EndpointKind e) {
            return String.format("%s \"%s.%s\"", clsName(e), e.getModel().getName(), e.getName());
        }
    }
    
    class Copy implements Block {
        public Target from, to;
        @Override public String toString() {
            return String.format("%s %s -> %s", clsName(this), from, to);
        }
    }
    
    /** Model instance */
    class Subcircuit implements Block, Named {
        public final Model model, outer;
        public final Namespace<SinkEnd> sinks = new Namespace<>(true);
        public final Namespace<SourceEnd> sources = new Namespace<>(true);
        
        public Subcircuit(Model m, Model o) {
            model = m;
            outer = o;
            for (var e : m.endpoints.values()) {
                if (e.isSource())
                    sources.rawPut(e.name, new SourceEnd((Model.SourceEnd) e));
                else
                    sinks.rawPut(e.name, new SinkEnd((Model.SinkEnd) e));
            }
        }
        public Stream<Endpoint<?>> getLinks() {
            return Stream.concat(sinks.values().stream(), sources.values().stream());
        }
        public boolean isUnused() {
            return sources.values().stream().allMatch(ReadableKind::isUnused);
        }
        @Override public String getName() { return model.name; }
        @Override public String toString() {
            return Named.desc(this);
        }
        
        public abstract class Endpoint<T extends Model.Endpoint> extends TargetProxy<T> implements EndpointKind{
            public final Subcircuit parent = Subcircuit.this;
            private Endpoint(T r)  { ref = r; }
            @Override public Model getModel()   { return model; }
            @Override public boolean isSource() { return ref.isSource(); }
            @Override public String toString()  { return EndpointKind.desc(this); }
        }
        public final class SinkEnd extends Endpoint<Model.SinkEnd> implements WritableKind.Impl {
            private Target link;
            SinkEnd(Model.SinkEnd r) { super(r); }
            @Override public void _link(Target t) { link = t; }
            @Override public Target getLinked() { return link; }
            @Override public boolean isUnused() { return WritableKind.Impl.super.isUnused(); }
        }
        public final class SourceEnd extends Endpoint<Model.SourceEnd> implements ReadableKind {
            private final LinkedHashSet<WritableKind> usages = new LinkedHashSet<>();
            private String raw;
            SourceEnd(Model.SourceEnd r) { super(r); }
            @Override public String getRaw() {
                if (isUnused())
                    throw new UnreachableError(String.format("%s must be read to have a temporary name.", this));
                if (raw == null)
                    raw = String.format("%d.%s",
                        // parent.getName(),
                        outer.blocks.indexOf(parent) + 1,
                        ref.getBLIFName()
                    );
                return raw;
            }
            @Override public Set<WritableKind> getReads() { return usages; }
            @Override public boolean isUnused() { return ReadableKind.super.isUnused(); }
        }
    }
    
    /** Model definition */
    class Model implements Named {
        public String name;
        public Source parent;
        public final Namespace<Endpoint> endpoints = new Namespace<>(true);
        public final List<Block> blocks = new ArrayList<>();
    
        public Stream<Endpoint> getSinks() {
            return stream(endpoints.values()).filter(Endpoint::isSink);
        }
        public Stream<Endpoint> getSources() {
            return stream(endpoints.values()).filter(Endpoint::isSource);
        }
        public Stream<Model> flatMap() {
            return Stream.concat(
                Stream.of(this),
                blocks.stream()
                    .map(b -> b instanceof Subcircuit s ? s.model : null)
                    .filter(Objects::nonNull)
                    .flatMap(Model::flatMap)
            ).distinct();
        }
        public boolean isExternal() {
            return blocks.isEmpty();
        }
        @Override public String getName() { return name; }
        @Override public String toString() {
            return Named.desc(this);
        }
    
        @RequiredArgsConstructor
        public abstract class Endpoint extends TargetObj implements EndpointKind {
            private final boolean isSource;
            @Override public Model getModel()   { return Model.this; }
            @Override public boolean isSource() { return isSource; }
            @Override public String toString() { return EndpointKind.desc(this); }
        }
        public final class SinkEnd extends Endpoint implements ReadableKind {
            private final LinkedHashSet<WritableKind> usages = new LinkedHashSet<>();
            SinkEnd() { super(false); }
            @Override public Set<WritableKind> getReads() { return usages; }
            @Override public boolean isUnused() { return ReadableKind.super.isUnused(); }
        }
        public final class SourceEnd extends Endpoint implements WritableKind.Impl {
            private Target link;
            SourceEnd() { super(true); }
            @Override public void _link(Target t) { link = t; }
            @Override public Target getLinked() { return link; }
            @Override public boolean isUnused() { return WritableKind.Impl.super.isUnused(); }
        }
    }
}
