parser grammar CLIFParser;
options { tokenVocab=CLIFLexer; }

@rulecatch { catch-blocks }


//-------- GLOBAL --------
source: directive*  inclusion*  (struct | union | model | kiss)*  EOF;

directive: AT NAME;
inclusion: INCLUDE FILEPATH FP_SEMIC;


//-------- COMMON --------
num: DECIM | HEXAD | OCTAL | BINAR;

varlist: var (COMMA var)*;
var: (type=NAME)? RAW? name=NAME;

ref: member | slice;
member: NAME (DOT NAME)*;
slice: member LBRAK a=num (COLON b=num)? RBRAK;


declare : struct | union | var SEMIC;
struct  : STRUCT RAW? NAME RBRACE declare+ LBRACE;
union   : UNION RAW? NAME RBRACE declare+ LBRACE;


//-------- MODEL --------
model: MODEL NAME  LPAR inputs=varlist? RPAR  (ARROW outputs=varlist)?  model_body;

model_body : LBRACE (model_stmt? SEMIC)* RBRACE
           |        (model_stmt? SEMIC)+
           ;
model_stmt : member SET exp                               # modAssign
           | nam=NAME SET  NEW mod=NAME  LPAR args? RPAR  # modSubckt
           | STATIC RAW var (SET exp)?                    # modLatch
           | model_if                                     # modIf
           | BREAK                                        # modBreak
           ;

model_if: <assoc=right>
          IF LPAR cond+=exp RPAR blk+=model_body
          (ELSE IF LPAR cond+=exp RPAR blk+=model_body)*
          (ELSE blk+=model_body)?;

args: arglist | argmap | arglist COMMA argmap;
arglist: exp (COMMA exp)*;
argmap: argset (COMMA argset)*;
argset: NAME SET exp;

exp : num                                     # expNumber
    | BOOL                                    # expBool
    | ref                                     # expRef
    | LPAR exp RPAR                           # expParens
    //
    | <assoc=right> NOT exp                   # expNot
    | exp op=(AST | DIV | MOD) exp            # expMul
    | exp op=(PLUS | MINUS) exp               # expSum
    //
    | <assoc=right> exp op=CONCAT exp         # expConcat
    | exp op=(LSHIFT | RSHIFT) exp            # expShift
    //
    | exp op=(LESS | LEQ | GEQ | GREAT) exp   # expUnequal
    | exp op=(EQU | NEQ) exp                  # expEqual
    | exp op=AND exp                          # expAnd
    | exp op=XOR exp                          # expXor
    | exp op=OR exp                           # expOr
    //
    | <assoc=right> exp QUEST exp COLON exp   # expMux
    ;


//-------- KISS --------
kiss: KISS NAME  LPAR inputs=varlist? RPAR  (ARROW outputs=varlist)?  body=kiss_body;

kiss_body : LBRACE (kiss_stmt? SEMIC)* RBRACE
          |        (kiss_stmt? SEMIC)+
          ;
kiss_stmt : CASE NAME COLON            # kissCase
          | BREAK NAME?                # kissBreak
          ;
