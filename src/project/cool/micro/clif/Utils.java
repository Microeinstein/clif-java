package cool.micro.clif;

import java.nio.file.*;
import java.util.*;
import java.util.function.*;
import java.util.stream.*;

import org.antlr.v4.runtime.*;


public interface Utils {
    Path PWD = Path.of(System.getProperty("user.dir"));

    static void dbg(Object... objs) {
        Arrays.stream(objs).forEach(System.err::print);
        System.err.println();
    }
    static String relpath(Path p) {
        return PWD.relativize(p).toString();
    }
    
    static <T> void interleave(Iterator<T> it, BiConsumer<Boolean, T> acc) {
        var space = false;
        while (it.hasNext()) {
            acc.accept(space, it.next());
            space = true;
        }
    }
    static <T> void interleave(Iterator<T> it, Consumer<T> acc, Runnable med) {
        interleave(it, (space, el) -> {
            if (space)
                med.run();
            acc.accept(el);
        });
    }
    static <T> void interleave(Iterable<T> it, Consumer<T> acc, Runnable med) {
        interleave(it.iterator(), acc, med);
    }
    static <T> void interleave(Stream<T> st, Consumer<T> acc, Runnable med) {
        interleave(st.iterator(), acc, med);
    }
    static <T> Iterable<T> iter(Iterator<T> it) {
        return () -> it;
    }
    static <T> Iterable<T> iter(Stream<T> st) {
        return st::iterator;
    }
    static <K,V> Iterable<Map.Entry<K,V>> iter(Map<K,V> m) {
        return iter(m.entrySet().iterator());
    }
    static <T> Stream<T> stream(Iterable<T> it) {
        return StreamSupport.stream(it.spliterator(), false);
    }
    
    static int sizeof(Object x) {
        if (x instanceof Collection<?> c)
            return c.size();
        if (x instanceof Map<?,?> m)
            return m.size();
        throw new RuntimeException();
    }
    static int sizeofnum(int num) {
        if (num == 0)
            return 1;
        return 31 - Integer.numberOfLeadingZeros(num);
    }
    
    static String optional(Token tok) {
        return tok != null ? tok.getText() : null;
    }
    static String optional(ParserRuleContext ctx) {
        return ctx != null ? ctx.getText() : null;
    }
    
    static String clsName(Object o) {
        if (o == null)
            return "(undefined)";
        var cn = o.getClass().getSimpleName();
        if (cn.equals(""))
            return "(anonymous)";
        return cn;
    }
}
