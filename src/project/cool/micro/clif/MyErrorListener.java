package cool.micro.clif;

import java.nio.file.*;
import java.util.BitSet;

import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.atn.ATNConfigSet;
import org.antlr.v4.runtime.dfa.DFA;

import static cool.micro.clif.MyExceptions.*;


public class MyErrorListener extends ConsoleErrorListener {
    private final SourceReader reader;

    public MyErrorListener(SourceReader reader) {
        super();
        this.reader = reader;
    }
    private void fail() {
        throw (reader.getCurrent().lastError = new ParsingError());
    }
    private void errPrintPos(int line, int x, Path file) {
        System.err.printf("@%d:%d of %s\n", line, x, file != null ? file.toString() : "(stream)");
    }

    @Override
    public void syntaxError(
            Recognizer<?, ?> recognizer,
            Object offendingSymbol,
            int line,
            int charPositionInLine,
            String msg,
            RecognitionException e
    ) {
        System.err.printf("[syntax] %s\n", msg);
        errPrintPos(line, charPositionInLine, reader.getCurrent().path);
        //super.syntaxError(recognizer, offendingSymbol, line, charPositionInLine, msg, e);
        fail();
    }

    @Override
    public void reportAmbiguity(
            Parser recognizer,
            DFA dfa,
            int startIndex,
            int stopIndex,
            boolean exact,
            BitSet ambigAlts,
            ATNConfigSet configs
    ) {
        System.err.println("[ambiguity] error");
        super.reportAmbiguity(recognizer, dfa, startIndex, stopIndex, exact, ambigAlts, configs);
        fail();
    }

    @Override
    public void reportAttemptingFullContext(
            Parser recognizer,
            DFA dfa,
            int startIndex,
            int stopIndex,
            BitSet conflictingAlts,
            ATNConfigSet configs
    ) {
        System.err.println("[full context] error");
        super.reportAttemptingFullContext(recognizer, dfa, startIndex, stopIndex, conflictingAlts, configs);
        fail();
    }

    @Override
    public void reportContextSensitivity(
            Parser recognizer,
            DFA dfa,
            int startIndex,
            int stopIndex,
            int prediction,
            ATNConfigSet configs
    ) {
        System.err.println("[context sensitivity] error");
        super.reportContextSensitivity(recognizer, dfa, startIndex, stopIndex, prediction, configs);
        fail();
    }
}
