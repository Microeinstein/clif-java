package cool.micro.clif;

import java.io.*;
import java.nio.file.*;
import java.util.*;

import org.antlr.v4.runtime.*;

import cool.micro.clif.gen.*;
import static cool.micro.clif.SourceMapping.*;
import static cool.micro.clif.MyExceptions.*;


@SuppressWarnings({"UnusedReturnValue", "SameParameterValue"})
public class SourceReader {
    private Source main;
    public final HashSet<Source> sources = new HashSet<>();
    private final Stack<Source> working = new Stack<>();

    public Source getCurrent() { return working.peek(); }
    public Source getMain() { return main; }

    public Source readFile(String path) {
        return readFile(Path.of(path).toAbsolutePath());
    }
    public Source readFile(Path path) {
        return readFile(path, null);
    }
    Source readFile(Path path, Source requiring) {
        if (!path.toFile().exists()) {
            System.err.println("File not found: " + path);
            return null;
        }
        var req = new Source(path);

        CharStream source;
        try {
            source = CharStreams.fromPath(path);
        } catch (IOException e) {
            System.err.println("Unable to open file: " + path);
            throw new RuntimeException();
        }
        return main = readSource(source, req, requiring);
    }

    public Source readStream(InputStream input) {
        return readStream(input, null);
    }
    Source readStream(InputStream input, Source requiring) {
        var req = new Source(null);

        CharStream source;
        try {
            source = CharStreams.fromStream(input);
        } catch (IOException e) {
            System.err.println("Unable to open stream.");
            throw new RuntimeException();
        }
        return main = readSource(source, req, requiring);
    }

    private Source readSource(CharStream source, Source required, Source requiring) {
        if (required.path != null) {
            var existing = sources.stream().filter(s -> s.samePath(required)).findFirst();
            if (existing.isPresent()) {
                // Source will exists on second inclusion globally (either different or same source)
                var ex = existing.get();
                if (requiring != null && ex.includes.contains(requiring)) {
                    // recursive inclusion
                    throw new RecursiveInclusionEx(String.format(
                        "BLIF does not support recursive source inclusions.\n  '%s' <- '%s'",
                        requiring.path.getFileName(), required.path.getFileName()
                    ));
                }
                return ex;
            }
        }

        var errHandler = new MyErrorListener(this);
        working.push(required);
        sources.add(required);
        // @myself: do not prioritize last - it's not a global dependency collection
        if (requiring != null)
            requiring.includes.add(required);

        // lexer phase
        var lexer = new CLIFLexer(source);
        lexer.removeErrorListeners();
        lexer.addErrorListener(errHandler);
        var tokenStream = new CommonTokenStream(lexer);

        // parsing phase
        var parser = new CLIFParser(tokenStream) {
            @Override protected void triggerExitRuleEvent() {
                // do not trigger exit rules on error
                if (getCurrent().lastError != null)
                    return;
                super.triggerExitRuleEvent();
            }

            @Override public String toString() {
                return Utils.relpath(required.path);
            }
        };
        parser.removeErrorListeners();
        parser.addErrorListener(errHandler);
        // parser.setBuildParseTree(false);
        CLIFParser.SourceContext ast = parser.source();

        // modelling phase
        // valid syntax here
        SourceMapper.map(this, required, ast);
        working.pop();
        return required;
    }
}
