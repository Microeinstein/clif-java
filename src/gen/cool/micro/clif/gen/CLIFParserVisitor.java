// Generated from java-escape by ANTLR 4.11.1
package cool.micro.clif.gen;
import org.antlr.v4.runtime.tree.ParseTreeVisitor;

/**
 * This interface defines a complete generic visitor for a parse tree produced
 * by {@link CLIFParser}.
 *
 * @param <T> The return type of the visit operation. Use {@link Void} for
 * operations with no return type.
 */
public interface CLIFParserVisitor<T> extends ParseTreeVisitor<T> {
	/**
	 * Visit a parse tree produced by {@link CLIFParser#source}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSource(CLIFParser.SourceContext ctx);
	/**
	 * Visit a parse tree produced by {@link CLIFParser#directive}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDirective(CLIFParser.DirectiveContext ctx);
	/**
	 * Visit a parse tree produced by {@link CLIFParser#inclusion}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitInclusion(CLIFParser.InclusionContext ctx);
	/**
	 * Visit a parse tree produced by {@link CLIFParser#num}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitNum(CLIFParser.NumContext ctx);
	/**
	 * Visit a parse tree produced by {@link CLIFParser#varlist}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitVarlist(CLIFParser.VarlistContext ctx);
	/**
	 * Visit a parse tree produced by {@link CLIFParser#var}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitVar(CLIFParser.VarContext ctx);
	/**
	 * Visit a parse tree produced by {@link CLIFParser#ref}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitRef(CLIFParser.RefContext ctx);
	/**
	 * Visit a parse tree produced by {@link CLIFParser#member}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitMember(CLIFParser.MemberContext ctx);
	/**
	 * Visit a parse tree produced by {@link CLIFParser#slice}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSlice(CLIFParser.SliceContext ctx);
	/**
	 * Visit a parse tree produced by {@link CLIFParser#declare}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDeclare(CLIFParser.DeclareContext ctx);
	/**
	 * Visit a parse tree produced by {@link CLIFParser#struct}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitStruct(CLIFParser.StructContext ctx);
	/**
	 * Visit a parse tree produced by {@link CLIFParser#union}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitUnion(CLIFParser.UnionContext ctx);
	/**
	 * Visit a parse tree produced by {@link CLIFParser#model}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitModel(CLIFParser.ModelContext ctx);
	/**
	 * Visit a parse tree produced by {@link CLIFParser#model_body}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitModel_body(CLIFParser.Model_bodyContext ctx);
	/**
	 * Visit a parse tree produced by the {@code modAssign}
	 * labeled alternative in {@link CLIFParser#model_stmt}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitModAssign(CLIFParser.ModAssignContext ctx);
	/**
	 * Visit a parse tree produced by the {@code modSubckt}
	 * labeled alternative in {@link CLIFParser#model_stmt}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitModSubckt(CLIFParser.ModSubcktContext ctx);
	/**
	 * Visit a parse tree produced by the {@code modLatch}
	 * labeled alternative in {@link CLIFParser#model_stmt}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitModLatch(CLIFParser.ModLatchContext ctx);
	/**
	 * Visit a parse tree produced by the {@code modIf}
	 * labeled alternative in {@link CLIFParser#model_stmt}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitModIf(CLIFParser.ModIfContext ctx);
	/**
	 * Visit a parse tree produced by the {@code modBreak}
	 * labeled alternative in {@link CLIFParser#model_stmt}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitModBreak(CLIFParser.ModBreakContext ctx);
	/**
	 * Visit a parse tree produced by {@link CLIFParser#model_if}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitModel_if(CLIFParser.Model_ifContext ctx);
	/**
	 * Visit a parse tree produced by {@link CLIFParser#args}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitArgs(CLIFParser.ArgsContext ctx);
	/**
	 * Visit a parse tree produced by {@link CLIFParser#arglist}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitArglist(CLIFParser.ArglistContext ctx);
	/**
	 * Visit a parse tree produced by {@link CLIFParser#argmap}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitArgmap(CLIFParser.ArgmapContext ctx);
	/**
	 * Visit a parse tree produced by {@link CLIFParser#argset}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitArgset(CLIFParser.ArgsetContext ctx);
	/**
	 * Visit a parse tree produced by the {@code expNumber}
	 * labeled alternative in {@link CLIFParser#exp}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExpNumber(CLIFParser.ExpNumberContext ctx);
	/**
	 * Visit a parse tree produced by the {@code expUnequal}
	 * labeled alternative in {@link CLIFParser#exp}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExpUnequal(CLIFParser.ExpUnequalContext ctx);
	/**
	 * Visit a parse tree produced by the {@code expBool}
	 * labeled alternative in {@link CLIFParser#exp}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExpBool(CLIFParser.ExpBoolContext ctx);
	/**
	 * Visit a parse tree produced by the {@code expSum}
	 * labeled alternative in {@link CLIFParser#exp}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExpSum(CLIFParser.ExpSumContext ctx);
	/**
	 * Visit a parse tree produced by the {@code expShift}
	 * labeled alternative in {@link CLIFParser#exp}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExpShift(CLIFParser.ExpShiftContext ctx);
	/**
	 * Visit a parse tree produced by the {@code expOr}
	 * labeled alternative in {@link CLIFParser#exp}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExpOr(CLIFParser.ExpOrContext ctx);
	/**
	 * Visit a parse tree produced by the {@code expMul}
	 * labeled alternative in {@link CLIFParser#exp}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExpMul(CLIFParser.ExpMulContext ctx);
	/**
	 * Visit a parse tree produced by the {@code expParens}
	 * labeled alternative in {@link CLIFParser#exp}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExpParens(CLIFParser.ExpParensContext ctx);
	/**
	 * Visit a parse tree produced by the {@code expAnd}
	 * labeled alternative in {@link CLIFParser#exp}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExpAnd(CLIFParser.ExpAndContext ctx);
	/**
	 * Visit a parse tree produced by the {@code expEqual}
	 * labeled alternative in {@link CLIFParser#exp}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExpEqual(CLIFParser.ExpEqualContext ctx);
	/**
	 * Visit a parse tree produced by the {@code expXor}
	 * labeled alternative in {@link CLIFParser#exp}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExpXor(CLIFParser.ExpXorContext ctx);
	/**
	 * Visit a parse tree produced by the {@code expConcat}
	 * labeled alternative in {@link CLIFParser#exp}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExpConcat(CLIFParser.ExpConcatContext ctx);
	/**
	 * Visit a parse tree produced by the {@code expMux}
	 * labeled alternative in {@link CLIFParser#exp}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExpMux(CLIFParser.ExpMuxContext ctx);
	/**
	 * Visit a parse tree produced by the {@code expRef}
	 * labeled alternative in {@link CLIFParser#exp}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExpRef(CLIFParser.ExpRefContext ctx);
	/**
	 * Visit a parse tree produced by the {@code expNot}
	 * labeled alternative in {@link CLIFParser#exp}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExpNot(CLIFParser.ExpNotContext ctx);
	/**
	 * Visit a parse tree produced by {@link CLIFParser#kiss}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitKiss(CLIFParser.KissContext ctx);
	/**
	 * Visit a parse tree produced by {@link CLIFParser#kiss_body}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitKiss_body(CLIFParser.Kiss_bodyContext ctx);
	/**
	 * Visit a parse tree produced by the {@code kissCase}
	 * labeled alternative in {@link CLIFParser#kiss_stmt}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitKissCase(CLIFParser.KissCaseContext ctx);
	/**
	 * Visit a parse tree produced by the {@code kissBreak}
	 * labeled alternative in {@link CLIFParser#kiss_stmt}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitKissBreak(CLIFParser.KissBreakContext ctx);
}