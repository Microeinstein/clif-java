package cool.micro.clif;

import lombok.experimental.StandardException;


public interface MyExceptions {
    @StandardException class FileException extends RuntimeException {}
    @StandardException class CloseException extends RuntimeException {}
    
    @StandardException class UnreachableError extends RuntimeException {}
    @StandardException class NotImplementedError extends RuntimeException {}
    @StandardException class ParsingError extends RuntimeException {}
    @StandardException class RecursiveInclusionEx extends RuntimeException {}
    @StandardException class TypeError extends RuntimeException {}
    @StandardException class MemberAccessError extends RuntimeException {}
    @StandardException class ReferenceError extends RuntimeException {}
    @StandardException class BindingError extends RuntimeException {}
}
