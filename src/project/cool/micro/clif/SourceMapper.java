package cool.micro.clif;

import java.nio.file.Path;
import java.util.*;
import java.util.function.*;

import lombok.*;
import org.antlr.v4.runtime.*;

import cool.micro.clif.gen.*;

import static cool.micro.clif.gen.CLIFParser.*;
import static cool.micro.clif.Utils.*;
import static cool.micro.clif.MyExceptions.*;
import static cool.micro.clif.SourceMapping.*;


// will be called in SourceReader for a single Source, after parsing phase
@RequiredArgsConstructor
class SourceMapper extends CLIFParserBaseVisitor<Object> {
    public final Source source;
    final SourceReader reader;
    final ModelHeaderMapper mhmap = new ModelHeaderMapper();
    
    public static void map(SourceReader r, Source s, SourceContext ctx) {
        new SourceMapper(s, r).visitSource(ctx);
    }
    
    @Override public Source visitSource(SourceContext ctx) {
        // read and discard returned Source (it will be handled in readSource)
        for (var ictx : ctx.inclusion())
            ictx.accept(this);
        
        // headers
        for (var mctx : ctx.model()) {
            var m = (Model)mctx.accept(mhmap);
            m.parent = source;
            source.models.put(m);
        }
        // bodies
        {
            var i1 = ctx.model().iterator();
            var i2 = source.models.values().iterator();
            while (i1.hasNext())
                i1.next().accept(new ModelMapper(i2.next()));
        }
        
        source.mapped = true;
        return source;
    }
    @Override public Source visitInclusion(InclusionContext ctx) {
        var path2 = ctx.FILEPATH().getText();
        Path other;
        if (source.path != null)
            other = source.path.resolveSibling(path2);
        else
            other = Utils.PWD.resolve(path2);
        return reader.readFile(other, source);
    }
    
    
    class ModelHeaderMapper extends CLIFParserBaseVisitor<Model> {
        private void putModelEndpoint(Model m, boolean isSource, VarContext ctx) {
            var e = isSource ? m.new SourceEnd() : m.new SinkEnd();
            e.name = ctx.name.getText();
            e.type = source.findType(optional(ctx.type));
            m.endpoints.put(e);
        }
        @Override public Model visitModel(ModelContext ctx) {
            var m = new Model();
            m.parent = source;
            m.name = ctx.NAME().getText();
            if (ctx.inputs != null)
                ctx.inputs.var().forEach(g -> putModelEndpoint(m, false, g));
            if (ctx.outputs != null)
                ctx.outputs.var().forEach(g -> putModelEndpoint(m, true, g));
            return m;
        }
    }
    
    
    @RequiredArgsConstructor
    class ModelMapper extends CLIFParserBaseVisitor<Void> {
        private final Model model;
        private final Namespace<Named> symbols = new Namespace<>();  // const, alias, slice, subcircuit, endpointkind
        private final Set<Const> reqConsts = new LinkedHashSet<>();
        private final ReferenceMapper refmap = new ReferenceMapper();
        private final ExprMapper expmap = new ExprMapper();
    
        @Override public Void visitModel(ModelContext ctx) {
            // initial i/o endpoints
            for (var e : model.endpoints.values())
                symbols.put(e.name, e);
            
            // convert model statements
            for (var g : ctx.model_body().model_stmt())
                g.accept(this);
            
            // remainings
            model.blocks.addAll(0, reqConsts);
            dbg("[bindings]");
            for (var e : model.endpoints.values()) {
                if (!(e instanceof WritableKind we))
                    continue;
                var t = we.getLinked();
                dbg(e, " <- ", t);
                // proxies: slice, connector, custom type member, ...?
                if (!(t instanceof ReadableKind re) || re.optimized() != null || !re.getReads().contains(e))
                    continue;
                var cp = new Copy();
                cp.from = t;
                cp.to = e;
                model.blocks.add(cp);
            }
            
            // optimize
            dbg("[blocks]");
            Function<Block,Boolean> filt = (b) -> {
                if (b instanceof Subcircuit sub)
                    return sub.isUnused();
                return false;
            };
            model.blocks.removeIf(b -> {
                var ret = filt.apply(b);
                dbg(ret ? "x " : "- ", b);
                return ret;
            });
            return null;
        }
        @Override public Void visitModAssign(ModAssignContext ctx) {
            var name = ctx.member().getText();  // HACK find proper name
            
            // proxies: slice, custom type member, ...?
            var left = refmap.mapLeftValue(ctx.member());
            // if (left != null)
            //     left = left.getProxied();
            var right = ctx.exp().accept(expmap);
            
            // bind or label
            if (left instanceof WritableKind we) {
                if (right instanceof Target targ)
                    we.link(targ);
                else throw new BindingError(String.format("Unable to bind entire %s to an endpoint.", clsName(right)));
                return null;
            }
            // else simple alias
            // yes: null, const, subcircuit, connector, ...
            //  no: slice, endpointkind, ...
            symbols.put(name, right);
            return null;
        }
        @Override public Void visitModSubckt(ModSubcktContext ctx) {
            var local = ctx.nam.getText();
            var mname = ctx.mod.getText();
            var m = source.models.getOrFallback(mname);
            if (m == null)
                throw new ReferenceError(String.format("Unknown model '%s'.", mname));
            if (m.flatMap().anyMatch(m2 -> m2 == model))
                throw new RecursiveInclusionEx(String.format(
                    "BLIF does not support recursive model inclusions.\n  '%s' <- '%s'",
                    model.getName(), m.getName()
                ));
            var s = new Subcircuit(m, model);
            if (ctx.args() != null)
                ctx.args().accept(new ArgsMapper(s));
            for (var e : s.sinks.values()) {
                if (e.isUnused())
                    e.link(Const.ZERO);
            }
            symbols.put(local, s);
            model.blocks.add(s);
            return null;
        }
        
        
        @RequiredArgsConstructor
        class ArgsMapper extends CLIFParserBaseVisitor<Void> {
            private final Subcircuit subckt;
            private Iterator<Subcircuit.SinkEnd> iter;
        
            private Target getExpr(ExpContext ctx) {
                var obj = ctx.accept(expmap);
                if (obj instanceof Target targ)
                    return targ;
                throw new ReferenceError(String.format("%s cannot be a parameter.", clsName(obj)));
            }
            @Override public Void visitArgs(ArgsContext ctx) {
                iter = subckt.sinks.values().iterator();
                super.visitArgs(ctx);
                return null;
            }
            @Override public Void visitArglist(ArglistContext ctx) {
                for (var ec : ctx.exp()) {
                    if (!iter.hasNext())
                        throw new ReferenceError("No more positional parameters.");
                    var e = iter.next();
                    var ex = getExpr(ec);
                    e.link(ex);
                    if (ex.getSize() < e.getSize())
                        reqConsts.add(Const.ZERO);
                }
                return null;
            }
            @Override public Void visitArgset(ArgsetContext ctx) {
                var n = ctx.NAME().getText();
                var e = subckt.sinks.lookup(n);
                if (e == null)
                    throw new ReferenceError(String.format("'%s' does not contain '%s' input.", subckt.getName(), n));
                var ex = getExpr(ctx.exp());
                e.link(ex);
                return null;
            }
        }
        
        
        class ReferenceMapper extends CLIFParserBaseVisitor<Named> {
            private boolean resolve = false;
        
            public Target mapLeftValue(ParserRuleContext ctx) {
                resolve = false;
                return (Target)ctx.accept(this);
            }
            public Named mapRightValue(ParserRuleContext ctx) {
                resolve = true;
                return ctx.accept(this);
            }
            @Override public Named visitMember(MemberContext ctx) {
                var it = ctx.NAME().iterator();
                var ref = symbols.lookup(it.next().getText());
                if (resolve) {
                    if (ref instanceof WritableKind we)
                        return we.getLinked();
                }
                for (var name : iter(it)) {
                    Object tmp = ref;
                    if (tmp instanceof Subcircuit sub)
                        tmp = sub.sources;
                    // TODO custom types
                    if (tmp instanceof Namespace<?> ns) {
                        ref = ns.lookup(name.getText());
                        continue;
                    }
                    // handles null case
                    throw new MemberAccessError(String.format("Trying to access child member of %s.", clsName(tmp)));
                }
                if (resolve && ref == null)
                    throw new ReferenceError(String.format("'%s' is undefined.", ctx.getText()));
                return ref;
            }
            @Override public Slice visitSlice(SliceContext ctx) {
                if (!resolve)
                    throw new NotImplementedError();
                var obj = ctx.member().accept(this);
                if (!(obj instanceof Target))
                    throw new MemberAccessError(String.format("Trying to slice %s.", clsName(obj)));
                var sl = new Slice();
                sl.ref = (Target)obj;
                sl.start = Const.getNumber(ctx.a.getText());
                var s_end = Const.getNumber(Objects.requireNonNullElse(optional(ctx.b), "-1"));
                var t_max = sl.getEnd();
                sl.end = s_end < 0 ? (t_max + s_end + 1) : s_end;
                return sl;
            }
        }
    
    
        class ExprMapper extends CLIFParserBaseVisitor<Named> {
            @Override public Const visitExpNumber(ExpNumberContext ctx) {
                var c = Const.get(ctx.num().getText());
                reqConsts.add(c);
                return c;
            }
            @Override public Const visitExpBool(ExpBoolContext ctx) {
                var yes = ctx.getText().equalsIgnoreCase("true");
                var c = Const.get(yes ? "1" : "0");
                reqConsts.add(c);
                return c;
            }
            @Override public Named visitExpRef(ExpRefContext ctx) {
                return refmap.mapRightValue(ctx.ref());
            }
            @Override public Target visitExpConcat(ExpConcatContext ctx) {
                return null;  // TODO
            }
        }
    }
}
